weapons/lotusbang				LOTFIR		//Courtesy of 1337spy
weapons/lotusquiet				LOTSIL		//Courtesy of 1337spy
$alias weapons/lotusopen		weapons/deinoopen
$alias weapons/lotusclose		weapons/deinoclose
$alias weapons/lotusload		weapons/deinoload
$alias weapons/lotuseject		weapons/deinoeject
$alias weapons/lotusclick		weapons/deinoclick
$alias weapons/lotuscyl			weapons/deinocyl $volume weapons/lotuscyl 0.3

weapons/otisblast1  OTISFIRE
weapons/otisblast2  dsshtfar
weapons/otisclick   OTISDRYF
weapons/otiseject   OTISEJEC
weapons/otiscyl     OTISCOCK  $volume weapons/otiscyl 0.3
weapons/otisload    OTISINSR
weapons/otisopen    OTISOPEN
weapons/otisclose   OTISCLOS

$alias weapons/lotusblast1		weapons/deinoblast1
$alias weapons/lotusblast2		weapons/deinoblast2
$alias weapons/lotusclick		weapons/deinoclick
$alias weapons/lotuseject		weapons/deinoeject
$alias weapons/lotuscyl			weapons/deinocyl
$alias weapons/lotusload		weapons/deinoload
$alias weapons/lotusopen		weapons/deinoopen
$alias weapons/lotusclose		weapons/deinoclose

weapons/bcfire   			MOUSER		//Courtesy of 1337spy
$alias weapons/bcquiet		weapons/lotusquiet $volume weapons/bcquiet 0.35
$alias weapons/bcload		weapons/rifleload
$alias weapons/bcclick		weapons/rifleclick	
$alias weapons/bcclick2		weapons/rifleclick2

$alias weapons/trog     weapons/rifle   $limit weapons/trog 0 //shootbang

$alias weapons/trogclick    weapons/rifleclick		//used for manually chambering new round (including during reload)
$alias weapons/trogclick2     weapons/rifleclick2		//used during reload
$alias weapons/trogunload  	   weapons/rifleload					//magout
$alias weapons/trogload        weapons/rifleload		//magin
$alias weapons/troggrenopen    weapons/grenopen	//used for opening up the gl
$alias weapons/troggrenload    weapons/grenreload	//loading the grenade in
$alias weapons/trogchamber     weapons/rifchamber 	//chambering a new round??? i didn't check how this was different from click

$random weapons/hlar     { weapons/hlar1 weapons/hlar2 weapons/hlar3 }   $limit weapons/hlar 0 //shootbang
$random weapons/hlargrenadeshot	{ weapons/hlargl1 weapons/hlargl2 }

weapons/hlarunload  	   ar1mgout					
weapons/hlarload        ar1mgin		
$alias weapons/hlargrenopen    weapons/grenopen	
$alias weapons/hlargrenload    weapons/grenreload	
weapons/hlarchamber     ar1bolt 	
weapons/hlar1		ar1hks1
weapons/hlar2		ar1hks2
weapons/hlar3		ar1hks3
weapons/hlargl1	ar1gl1
weapons/hlargl2		ar1gl2

weapons/rubycharge					MASECHAR	//Deux Ex (2000, Ion Storm)
weapons/rubyendlo					MASELOWE	//Deux Ex (2000, Ion Storm)
weapons/rubyendhi					MASEEND		//Deux Ex (2000, Ion Storm)
weapons/rubybeam					MASEFIR1	//Deux Ex (2000, Ion Storm)
weapons/rubyfirelo					MASELOWF	//Deux Ex (2000, Ion Storm)
weapons/rubyfirehi					MASEFIR2	//Deux Ex (2000, Ion Storm)
weapons/rubymaserpoweron			MASEPOW1	//Deux Ex (2000, Ion Storm)
weapons/rubymaserpoweroff			MASEPOW2	//Deux Ex (2000, Ion Storm)
//all the beam sounds will break without these
$pitchshift weapons/rubyfirelo		0
$limit weapons/rubyfirelo			0
$pitchshift weapons/rubyfirehi		0
$limit weapons/rubyfirehi			0
$limit weapons/rubybeam				0

weapons/hmpholy					DSSHINE			//Courtesy of 1337spy
weapons/hmpfire					HELZFIRE		//Courtesy of 1337spy
weapons/hmpfireholy				HELZFRHL		//Courtesy of 1337spy
$alias weapons/hmpblast			weapons/deinoblast2
$alias weapons/hmpchamber1		weapons/pischamber1
$alias weapons/hmpchamber2		weapons/pischamber2
$alias weapons/hmpmagclick		weapons/pismagclick
$alias weapons/bottleload		potion/swish

//It's just the gunshot from CarnEvil
weapons/greelyshot						CARNSHOT $volume weapons/greelyshot 3.0//technically not a shotgun sound, but cmon I gotta
$alias weapons/greelyblast				weapons/deinoblast2
$alias weapons/greelyreload				weapons/huntreload
$alias weapons/greelyrackup				weapons/huntrackup
$alias weapons/greelyrackdown			weapons/huntrackdown

$alias weapons/guillotine weapons/pistol
$alias weapons/guilmagclick  weapons/pismagclick
$alias weapons/guildry    weapons/pistoldry
$alias weapons/guilchamber1  weapons/pischamber1
$alias weapons/guilchamber2  weapons/pischamber2

weapons/vera 						VERAFIRE
$alias weapons/verabelt			weapons/vulcbelt
$alias weapons/veramag			weapons/vulcmag
$alias weapons/veraumag		weapons/vulcumag
$alias weapons/verachamber	weapons/vulcchamber
$alias weapons/veraanup			weapons/vulcanup
$alias weapons/veraandown		weapons/vulcandown
$alias weapons/veraanon			weapons/vulcanon
$alias weapons/verashunt		weapons/vulcshunt
$alias weapons/veraopen1		weapons/vulcopen1
$alias weapons/veraopen2		weapons/vulcopen2
$alias weapons/veraextract		weapons/vulcextract
$alias weapons/veraforcemag	weapons/vulcforcemag
$random weapons/verafix {weapons/verashunt weapons/veraforcemag weapons/veraumag weapons/verabelt}
$random weapons/veratryfix {weapons/verashunt weapons/veraforcemag}
$random weapons/veratryfix2 {weapons/verashunt weapons/veraopen1 weapons/veraumag weapons/veraforcemag weapons/veraextract weapons/bigcrack weapons/pocket}

weapons/wiseaufire			wisfire		//Courtesy of 1337spy
$alias weapons/wiseauload	weapons/pischamber2
$alias weapons/wiseauzap	misc/arczap

Oddball/NormalShot				ODBF1
Oddball/WeakShot				ODBF2
OddBall/Crank					ODBSPN
Oddball/Woopsie					ODBFUP
OddBall/PumpUp					ODPUP
OddBall/PumpDown				ODPDN

$alias weapons/lisazap			misc/arczap
$alias weapons/lisafire			weapons/wiseaufire
$alias weapons/lisaload			weapons/pischamber2

weapons/spacelube					bstslip
weapons/bastardfire					BASTARDF
$alias weapons/bastardload			weapons/trogload
$alias weapons/bastardclick			weapons/trogclick
$alias weapons/bastardclick2		weapons/trogclick2
$alias weapons/bastgrenopen			weapons/troggrenopen
$alias weapons/bastgrenload			weapons/troggrenload

weapons/bp90fire							p90fire	//Courtesy of 1337spy
$alias weapons/bp90chamber					weapons/rifchamber
$alias weapons/bp90unload					weapons/trogunload
$alias weapons/bp90load						weapons/trogload
$alias weapons/bp90click					weapons/trogclick
$alias weapons/bp90click2					weapons/trogclick2

//Special thanks to 1337spy, who does a shitload of sound design work on weapons and did weapon sounds for HDest prpper.