// ------------------------------------------------------------
// Bastard Rifle
// ------------------------------------------------------------
/* While it has gone on record that Lt. Krognar Lodyte designed
the TRO-G, after his death and later resurrection as a cyborg,
Lt. Krognar found the small magazine capacity of the TRO-G had
gotten his new body destroyed on several instances, mainly when 
dealing with larger demons such as Hell Knights in close quarters.

After enough of these incidents of being scrapped and repaired,
several eyewitnesses reported the Lieutenant had grabbed one of the
mechanics, stating "You're going to help me make a gun better
suited for killing those goat bastards. I'm not gonna get
into anymore debt with these assholes than I already have."

After a few weeks with a leftover TRO-G and much table flipping,
the Bastard had been brought into this world. A far less elegant
weapon, the Bastard's most notable feature is the built in feeder
mechanism, doing away with magazines, the rifle is fed stray 4.26
rounds and is capable of holding roughly 100 rounds at any given 
time. Alongside this, the Bastard has a modified firing mechanism,
sacrificing the full auto firemode in favor of the hyperburst similar
to the now mainstream ZM66, but without the risk of a cookoff thanks
to the vented barrel and the ammo separation of the hopper system.

It should be noted however that due to this bizarre configuration 
and using fragile 4mil cartridges, there is always a small chance
of the weapon breaking ammunition while being fired. The Bastard is
durable enough to handle breakages without worry of jams or cookoffs,
but ammo waste is a common side effect of the weapon.

Like the TRO-G, the Bastard also features an integrated grenade  
launcher with the same trigger group. A deliberate choice from 
Krognar as he has grown accustomed to the hair trigger grenade launcher
configuration.

Naturally this weapon was not licensed or endorsed by Volt whatsoever,
making the rifle and the story behind it another strange anecdote in Volt's
history, though Volt themselves have refused to provide any further commentary.
*/
const HDLD_BAST="bst";

class HDBastardRifle:HDWeapon{
	default{

		weapon.selectionorder 35;
		weapon.slotnumber 4;
		weapon.slotpriority 1.6;
		inventory.pickupsound "misc/w_pkup";
		inventory.pickupmessage "You found a Bastard Rifle! What a crude thing...";
		scale 0.6;
		weapon.bobrangex 0.22;
		weapon.bobrangey 0.9;
		obituary "%o was assaulted by %k.";
		hdweapon.refid HDLD_BAST;
		tag "Bastard Rifle";
		inventory.icon "BSPAA0";
	}
	override void tick(){
		super.tick();
		drainheat(BASTS_HEAT,18);
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void postbeginplay(){
		super.postbeginplay();
		barrellength=29; //look at the sprites - GL does not extend beyond muzzle
			barrelwidth=1;
			barreldepth=3;
			weaponspecial=1337;
	}
	override double gunmass(){
			return 7.7+weaponstatus[BASTS_MAG]*0.01+(weaponstatus[0]&BASTF_GRENADELOADED?1.:0.);
	}
	override string,double getpickupsprite(){
		string spr;
		spr="BSPA";
		spr=spr.."A";
		return spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			sb.drawimage("RCLSA3A7",(-47,-10),sb.DI_SCREEN_CENTER_BOTTOM,scale:(2.1,2.55));
			sb.drawnum(hpl.countinv("FourMilAmmo"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			sb.drawimage("ROQPA0",(-68,-4),sb.DI_SCREEN_CENTER_BOTTOM,scale:(0.6,0.6));
			sb.drawnum(hpl.countinv("HDRocketAmmo"),-62,-8,sb.DI_SCREEN_CENTER_BOTTOM);

		}

		sb.drawwepcounter(hdw.weaponstatus[BASTS_AUTO],
			-22,-10,"RBRSA3A7","STBURAUT"
		);
		if(hdw.weaponstatus[0]&BASTF_GRENADELOADED)sb.drawrect(-20,-14,4,2.6);
		int lod=clamp(hdw.weaponstatus[BASTS_MAG]%100,0,95);
		sb.drawwepnum(lod,95);
		if(hdw.weaponstatus[0]&BASTF_CHAMBER){
			sb.drawrect(-19,-10,3,1);
			lod++;
		}
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.."  Shoot GL\n"
		..WEPHELP_RELOAD.."  Reload mag\n"
		..WEPHELP_ALTRELOAD.."	Reload GL\n"
		..WEPHELP_FIREMODE.."  Semi/Burst\n"
		..WEPHELP_MAGMANAGER
		..WEPHELP_UNLOAD.." 'Unload' hopper \(Hold "..WEPHELP_FIREMODE.." to unload GL\)\n"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
			int cx,cy,cw,ch;
			[cx,cy,cw,ch]=screen.GetClipRect();
			sb.SetClipRect(
				-24+bob.x,-16+bob.y,40,32,
				sb.DI_SCREEN_CENTER
			);
			vector2 bobb=bob*3;
			bobb.y=clamp(bobb.y,-8,8);
			sb.drawimage(
				"9arftst",(0,-9)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				alpha:0.8
			);
			sb.SetClipRect(cx,cy,cw,ch);
			sb.drawimage(
				"backsite",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP
			);
	}
	override double weaponbulk(){
		double blx=170;
			if(weaponstatus[0]&BASTF_GRENADELOADED)blx+=ENC_ROCKETLOADED;
		int mgg=weaponstatus[BASTS_MAG];
		return blx+(mgg<0?0:(mgg*ENC_426_LOADED));
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
				double angchange=-10;
				if(angchange)owner.angle-=angchange;
				owner.A_DropInventory("FourMilAmmo",amt*25);
				if(angchange){
					owner.angle+=angchange*2;
					owner.A_DropInventory("HDRocketAmmo",amt);
					owner.angle-=angchange;
				
			}
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("FourMilAmmo");
		owner.A_GiveInventory("FourMilAmmo",95);
	}
	action void A_FireHDGLPG(){ //gross hardcoded thing because I don't know enough to do it with an event handler
		A_StartSound("weapons/grenadeshot",CHAN_WEAPON,CHANF_OVERLAP);
		let ggg=gyrogrenade(spawn("GyroGrenade",pos+(
				0,0,HDWeapon.GetShootOffset(
					self,invoker.barrellength,
					invoker.barrellength-HDCONST_SHOULDERTORADIUS
				)-2
			),
			ALLOW_REPLACE)
		);
		ggg.angle=angle;ggg.pitch=pitch-2;ggg.target=self;ggg.master=self;
		ggg.primed=false;
	}
	states{
	ready:
		BSTD A 1 {
		A_WeaponReady(WRF_ALL);
		if(invoker.weaponstatus[BASTS_AUTO]>1)invoker.weaponstatus[BASTS_AUTO]=1;
		}goto readyend;
	firemode:
		BSTD A 1{
			if(invoker.weaponstatus[BASTS_AUTO]>=1)invoker.weaponstatus[BASTS_AUTO]=0;  
			else invoker.weaponstatus[BASTS_AUTO]++;
			A_WeaponReady(WRF_NONE);
		}
	firemodehold:
		BSTD A 1{
			if(pressingunload()
			&&(invoker.weaponstatus[BASTS_FLAGS]&BASTF_GRENADELOADED)){
			if(invoker.weaponstatus[BASTS_AUTO]>=1)invoker.weaponstatus[BASTS_AUTO]=0;  
			else invoker.weaponstatus[BASTS_AUTO]++;
				invoker.weaponstatus[BASTS_FLAGS]|=BASTF_UNLOADONLY;
				setweaponstate("unloadgrenade");
			}else A_WeaponReady(WRF_NONE);
		}
		BSTD A 0 A_JumpIf(pressingfiremode()&&(invoker.weaponstatus[BASTS_FLAGS]&BASTF_GRENADELOADED),"firemodehold");
		goto nope;

	select0:
		BSTD A 0;
		goto select0big;
	deselect0:
		BSTD A 0;
		goto deselect0big;
	flash:
		BSTF A 1 bright{
			A_Light1();
			HDFlashAlpha(-16);
			A_StartSound("weapons/bastardfire",CHAN_WEAPON);
			A_ZoomRecoil(max(0.95,1.-0.05*min(invoker.weaponstatus[BASTS_AUTO],3)));

			//shoot the bullet
			//copypaste any changes to spawnshoot as well!
			double brnd=(invoker.weaponstatus[BASTS_HEAT]);
			HDBulletActor.FireBullet(self,"HDB_426",
				spread:brnd>1.2?brnd:0
			);

			A_MuzzleClimb(
				-frandom(0.1,0.1),-frandom(0,0.1),
				-0.2,-frandom(0.2,0.3),
				-frandom(0.2,0.8),-frandom(0.7,1.4)
			);

			invoker.weaponstatus[BASTS_FLAGS]&=~BASTF_CHAMBER;
			invoker.weaponstatus[BASTS_HEAT]+=random(3,5);
			A_AlertMonsters();
		}
		goto lightdone;


	fire:
		BSTD A 0{
			if(invoker.weaponstatus[BASTS_AUTO]>0)A_SetTics(1);
		}goto shootgun;
	hold:
		BSTD A 0 A_JumpIf(invoker.weaponstatus[BASTS_AUTO]>3,"nope");
		BSTD A 0 A_JumpIf(invoker.weaponstatus[BASTS_AUTO],"shootgun");
	althold:
		---- A 1;
		---- A 0 A_Refire();
		goto ready;
	shootgun:
		BSTD A 1{
			if(
					!(invoker.weaponstatus[BASTS_FLAGS]&BASTF_CHAMBER)
					&&invoker.weaponstatus[BASTS_MAG]<1
			){
				setweaponstate("nope");
			}else if(!(invoker.weaponstatus[BASTS_FLAGS]&BASTF_CHAMBER)){
				//no shot but can chamber
				setweaponstate("chamber_manual");
			}else{
				A_GunFlash();
				A_WeaponReady(WRF_NONE);
				if(invoker.weaponstatus[BASTS_AUTO]>=1)invoker.weaponstatus[BASTS_AUTO]++;  
			}
		}
	chamber:
		BSTD D 0 offset(0,32){
			if(invoker.weaponstatus[BASTS_MAG]<1){
				setweaponstate("nope");
				return;
			}
			if(invoker.weaponstatus[BASTS_MAG]%100>0){  
				invoker.weaponstatus[BASTS_MAG]--;
				invoker.weaponstatus[BASTS_FLAGS]|=BASTF_CHAMBER;
			}else{
				invoker.weaponstatus[BASTS_MAG]=min(invoker.weaponstatus[BASTS_MAG],0);
				A_StartSound("weapons/rifchamber",CHAN_WEAPON,CHANF_OVERLAP);
			}
			A_WeaponReady(WRF_NOFIRE); //not WRF_NONE: switch to drop during cookoff
		}
		BSTD D 0 A_JumpIf(invoker.weaponstatus[BASTS_AUTO]<1,"nope");
		BSTD D 0 A_JumpIf(invoker.weaponstatus[BASTS_AUTO]>3,"nope");
		BSTD D 2 A_JumpIf(invoker.weaponstatus[BASTS_AUTO]>1,"shootgun");
		BSTD D 0 A_Refire();
		goto ready;
	user3:
		RIFG A 0 A_MagManager("HD4mMag");
		goto ready;
	altreload:
	reload:
	reloadstart:
		BSTD A 1 offset(0,34);
		BSTD A 1 offset(2,36);
		BSTD A 1 offset(4,40);
		BSTD A 1 offset(8,42){
			A_StartSound("weapons/bastardclick2",8,CHANF_OVERLAP,0.9,pitch:0.95);
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
		}
		BSTD C 2 offset(14,46){
			A_StartSound("weapons/bastardload",8,CHANF_OVERLAP);
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
		}
		BSTD C 0{
			int mg=invoker.weaponstatus[BASTS_MAG];
			if(mg==95)setweaponstate("reloaddone");
			else setweaponstate("loadhand");
		}
	loadhand:
		BSTD C 0 A_JumpIfInventory("FourMilAmmo",1,"loadhandloop");
		goto reloaddone;
	loadhandloop:
		BSTD C 4{
			int hnd=min(
				countinv("FourMilAmmo"),5,
				95-invoker.weaponstatus[BASTS_MAG]
			);
			if(hnd<1){
				setweaponstate("reloaddone");
				return;
			}else{
				A_TakeInventory("FourMilAmmo",hnd,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[BASTS_HAND]=hnd;
				A_StartSound("weapons/pocket",9);
			}
		}
	loadone:
		BSTD C 2 offset(16,50) A_JumpIf(invoker.weaponstatus[BASTS_HAND]<1,"loadhandnext");
		BSTD C 0 offset(16,50) {
		if(invoker.weaponstatus[BASTS_HAND]==1||invoker.weaponstatus[BASTS_MAG]==94){
			A_SetTics(4);
			invoker.weaponstatus[BASTS_HAND]--;
			invoker.weaponstatus[BASTS_MAG]++;
			A_StartSound("weapons/bastardclick2",8);
			setweaponstate("loadhandnext");
			}
		}
		BSTD C 4 offset(14,46){
			invoker.weaponstatus[BASTS_HAND]-=2;
			invoker.weaponstatus[BASTS_MAG]+=2;
			A_StartSound("weapons/bastardclick2",8);
		}loop;
	loadhandnext:
		BSTD C 8 offset(16,48){
			if(
				PressingFire()||
				PressingAltFire()||
				PressingZoom()||
				!countinv("FourMilAmmo")	//don't strip clips automatically
			)setweaponstate("reloaddone");
			else A_StartSound("weapons/pocket",9);
		}goto loadhandloop;
	reloaddone:
		BSTD C 1 offset(4,40);
		BSTD C 1 offset(2,36);
		BSTD C 1 offset(0,34);
		goto nope;
	unload:
		BSTD C 0 {if(
				invoker.weaponstatus[BASTS_MAG]==0  
			){
				setweaponstate("nope");
			}
		}
		BSTD C 1 offset(0,34);
		BSTD C 1 offset(2,36);
		BSTD C 1 offset(4,40);
		BSTD C 2 offset(8,42){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/bastardclick2",8);
		}
		BSTD C 4 offset (14,46){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/bastardload",8);
		}
	unloadloop:
		BSTD C 4 offset(3,41){
				A_StartSound("weapons/bastardclick2",8);
				invoker.weaponstatus[BASTS_MAG]--;
				A_SpawnItemEx(
						"ZM66DroppedRound",cos(pitch)*8,0,height-7-sin(pitch)*8,
						cos(pitch)*cos(angle-40)*1+vel.x,
						cos(pitch)*sin(angle-40)*1+vel.y,
						-sin(pitch)*1+vel.z,
						0,SXF_ABSOLUTEMOMENTUM|
						SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
					);
				A_StartSound("weapons/spacelube",8);
		}
		BSTD C 2 offset(2,42);
		BSTD C 0{
			if(
				PressingReload()||
				PressingFire()||
				PressingAltFire()||
				PressingZoom()
			)setweaponstate("unloaddone");
		}
	unloaddone:
		BSTD C 2 offset(2,42);
		BSTD C 3 offset(3,41);
		BSTD C 1 offset(4,40) A_StartSound("weapons/bastardclick",8);
		BSTD C 1 offset(2,36);
		BSTD C 1 offset(0,34);
		goto ready;

	reloadend:
		BSTD D 1 offset(-11,39);
		BSTD A 1 offset(-8,37) A_MuzzleClimb(frandom(0.2,-2.4),frandom(0.2,-1.4));
		BSTD A 1 offset(-3,34);
		goto chamber_manual;

	chamber_manual:
		BSTD A 0 A_JumpIf(invoker.weaponstatus[BASTS_FLAGS]&BASTF_CHAMBER,"nope");
		BSTD A 3 offset(-1,36)A_WeaponBusy();
		BSTD D 4 offset(-3,42){
			if(!invoker.weaponstatus[BASTS_MAG]%100)invoker.weaponstatus[BASTS_MAG]=0;
			if(
				!(invoker.weaponstatus[BASTS_FLAGS]&BASTF_CHAMBER)
				&& !(invoker.weaponstatus[BASTS_FLAGS]&BASTF_CHAMBER)
				&& invoker.weaponstatus[BASTS_MAG]%100>0
			){
				A_StartSound("weapons/bastardclick",CHAN_WEAPON);
				invoker.weaponstatus[BASTS_MAG]--;
				invoker.weaponstatus[BASTS_FLAGS]|=BASTF_CHAMBER;
			}else setweaponstate("nope");
		}
		BSTD A 2 offset(-1,36);
		BSTD A 0 offset(0,34);
		goto nope;


	unloadchamber:
		BSTD A 1 offset(-3,34);
		BSTD A 1 offset(-9,39);
		BSTD D 3 offset(-19,44) A_MuzzleClimb(frandom(-0.4,0.4),frandom(-0.4,0.4));
		BSTD A 2 offset(-16,42){
			A_MuzzleClimb(frandom(-0.4,0.4),frandom(-0.4,0.4));
			if(
				invoker.weaponstatus[BASTS_FLAGS]&BASTF_CHAMBER
			){
				A_SpawnItemEx("ZM66DroppedRound",0,0,20,
					random(4,7),random(-2,2),random(-2,1),0,
					SXF_NOCHECKPOSITION
				);
				invoker.weaponstatus[BASTS_FLAGS]&=~BASTF_CHAMBER;
				A_StartSound("weapons/bastardclick2",CHAN_WEAPON,CHANF_OVERLAP);
			}
		}goto reloadend;

	nadeflash:
		TNT1 A 0 A_JumpIf(invoker.weaponstatus[BASTS_FLAGS]&BASTF_GRENADELOADED,1);
		stop;
		TNT1 A 2{
			A_FireHDGLPG();
			invoker.weaponstatus[BASTS_FLAGS]&=~BASTF_GRENADELOADED;
			A_StartSound("weapons/grenadeshot",CHAN_WEAPON);
			A_ZoomRecoil(0.95);
		}
		TNT1 A 2 A_MuzzleClimb(
			0,0,0,0,
			-1.2,-3.,
			-1.,-2.8
		);
		stop;


	altfire:
		BSTD A 0 A_JumpIf(invoker.weaponstatus[BASTS_FLAGS]&BASTF_GRENADELOADED,1);
		goto nope;
		BSTD D 2;
		BSTD D 3 A_Gunflash("nadeflash");
		goto nope;


	altreload:
		BSTD A 0{
			invoker.weaponstatus[BASTS_FLAGS]&=~BASTF_UNLOADONLY;
			if(
				!(invoker.weaponstatus[BASTS_FLAGS]&BASTF_GRENADELOADED)
				&&countinv("HDRocketAmmo")
			)setweaponstate("unloadgrenade");
		}goto nope;
	unloadgrenade:
		BSTD D 0{
			A_SetCrosshair(21);
			A_MuzzleClimb(-0.3,-0.3);
		}
		BSTD D 2 offset(0,34);
		BSTD D 1 offset(4,38){
			A_MuzzleClimb(-0.3,-0.3);
		}
		BSTD D 2 offset(8,48){
			A_StartSound("weapons/bastgrenopen",CHAN_WEAPon,CHANF_OVERLAP);
			A_MuzzleClimb(-0.3,-0.3);
			if(invoker.weaponstatus[BASTS_FLAGS]&BASTF_GRENADELOADED)A_StartSound("weapons/grenreload",CHAN_WEAPON);
		}
		BSTD D 8 offset(10,49){
			if(!(invoker.weaponstatus[BASTS_FLAGS]&BASTF_GRENADELOADED)){
				if(!(invoker.weaponstatus[BASTS_FLAGS]&BASTF_UNLOADONLY))A_SetTics(3);
				return;
			}
			invoker.weaponstatus[BASTS_FLAGS]&=~BASTF_GRENADELOADED;
			if(
				!PressingUnload()
				||A_JumpIfInventory("HDRocketAmmo",0,"null")
			){
				A_SpawnItemEx("HDRocketAmmo",
					cos(pitch)*10,0,height-10-10*sin(pitch),vel.x,vel.y,vel.z,0,
					SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}else{
				A_StartSound("weapons/pocket",CHAN_WEAPON,CHANF_OVERLAP);
				A_GiveInventory("HDRocketAmmo",1);
				A_MuzzleClimb(frandom(0.8,-0.2),frandom(0.4,-0.2));
			}
		}
		BSTD D 0 A_JumpIf(invoker.weaponstatus[BASTS_FLAGS]&BASTF_UNLOADONLY,"greloadend");
	loadgrenade:
		BSTD D 2 offset(10,50) A_StartSound("weapons/pocket",CHAN_WEAPON,CHANF_OVERLAP);
		BSTD DBB 5 offset(10,50) A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		BSTD D 15 offset(8,50){
			A_TakeInventory("HDRocketAmmo",1,TIF_NOTAKEINFINITE);
			invoker.weaponstatus[BASTS_FLAGS]|=BASTF_GRENADELOADED;
			A_StartSound("weapons/bastgrenload",CHAN_WEAPON);
		}
	greloadend:
		BSTD D 4 offset(4,44) A_StartSound("weapons/bastgrenopen",CHAN_WEAPON);
		BSTD D 1 offset(0,40);
		BSTD A 1 offset(0,34) A_MuzzleClimb(frandom(-2.4,0.2),frandom(-1.4,0.2));
		goto nope;

	spawn:
		BSPA DA 0;
		---- A 0;
	spawn2:
		---- A -1{
			//set sprite
			sprite=getspriteindex("BSPAA0");

			//set to no-mag frame
			if(invoker.weaponstatus[BASTS_MAG]<0){
				frame=3;
			}
		}
	}
	override inventory CreateTossable(int amt){
		let owner=self.owner;
		let zzz=HDBastardRifle(super.createtossable());
		if(!zzz)return null;
		zzz.target=owner;
		return zzz;
	}

	override void InitializeWepStats(bool idfa){
		weaponstatus[0]|=BASTF_CHAMBER;
		weaponstatus[0]|=BASTF_GRENADELOADED;
		weaponstatus[BASTS_MAG]=95;
		if(!idfa && !owner){
			weaponstatus[BASTS_AUTO]=0;
			weaponstatus[BASTS_HEAT]=0;
		}
	}
	override void loadoutconfigure(string input){
			weaponstatus[0]|=BASTF_CHAMBER;
			int firemode=getloadoutvar(input,"firemode",1);
			int glready=getloadoutvar(input,"glready",1);
			if(!glready){weaponstatus[0]&=~BASTF_GRENADELOADED;}
			else if(glready>=0) {weaponstatus[0]|=BASTF_GRENADELOADED;}
			if(firemode>=0){
				weaponstatus[BASTS_AUTO]=clamp(firemode,0,1);
			}
		if(
			!(weaponstatus[0]&BASTF_CHAMBER)
			&&weaponstatus[BASTS_MAG]>0
		){
			weaponstatus[0]|=BASTF_CHAMBER;
			weaponstatus[BASTS_MAG]--;
		}
	}
}

enum BASTstatus{
	BASTF_CHAMBER=1,
	BASTF_GRENADELOADED=2,
	BASTF_UNLOADONLY=4,
	BASTF_STILLPRESSINGRELOAD=8,

	BASTS_FLAGS=0,
	BASTS_MAG=1, //-1 is empty
	BASTS_AUTO=2, //2 is burst, 2-5 counts ratchet
	BASTS_HEAT=3,
	BASTS_HAND=4,
};