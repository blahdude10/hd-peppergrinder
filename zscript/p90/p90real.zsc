const HDLD_PRINCESS="owl";

class HDPrincess:HDWeapon{ //The actual gun representative of Breaker
	default{
		+hdweapon.norandombackpackspawn;
		+hdweapon.fitsinbackpack
		weapon.selectionorder 25;
		weapon.slotnumber 4;
		weapon.slotpriority 1.6;
		inventory.pickupsound "misc/w_pkup";
		inventory.pickupmessage "Picked up a real L&M BreakerTek p90, she's a beauty.";
		inventory.maxamount 3;
		scale 0.4;
		weapon.bobrangex 0.22;
		weapon.bobrangey 0.9;
		obituary "%o was scourged by %k.";
		hdweapon.refid HDLD_PRINCESS;
		tag "BreakerTek p90 Classic";
		inventory.icon "PNVPA0";
		hdweapon.barrelsize 19,0.5,1;
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void tick(){
		super.tick();
		drainheat(VRISS_HEAT,12);
	}
	override void postbeginplay(){
		super.postbeginplay();
			weaponspecial=1337; //weppin sling
	}
	override double gunmass(){
			return 6.0+weaponstatus[VRISS_MAG]*0.01;
	}
	override string,double getpickupsprite(){
		string spr;
		spr="PNVP";

		//set to no-mag frame
		if(weaponstatus[0]&VRISF_VRISKA)spr=(weaponstatus[VRISS_MAG]<0?spr.."D":spr.."C");
		else spr=(weaponstatus[VRISS_MAG]<0?spr.."B":spr.."A");;

		return spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HDNDMBigMag")));
			if(nextmagloaded>50){
				sb.drawimage("PNMGC0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM);
			}else if(nextmagloaded<1){
				sb.drawimage("PNMGA0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.);
			}else sb.drawbar(
				"PNMGC0","PNEMPTY", //full,empty
				nextmagloaded,50,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawnum(hpl.countinv("HDNDMBigMag"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}

		sb.drawwepcounter(hdw.weaponstatus[VRISS_AUTO],
			-22,-10,"RBRSA3A7","STFULAUT"
		);
		int lod=clamp(hdw.weaponstatus[VRISS_MAG],0,50);
		sb.drawwepnum(lod,50);
		if(hdw.weaponstatus[0]&VRISF_CHAMBER){
			sb.drawrect(-19,-10,3,1);
			lod++;
		}
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_RELOAD.."  Put mag in\n"
		..WEPHELP_FIREMODE.."  Semi/Auto\n"
		..WEPHELP_MAGMANAGER
		..WEPHELP_UNLOAD..(weaponstatus[0]&VRISF_JAMMED?" Unjam gun\n":" Take mag out\n")
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		sb.SetClipRect(
			-8+bob.x,-4+bob.y,16,16,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*3;
		bobb.y=clamp(bobb.y,-8,8);
		if(hdw.weaponstatus[0]&VRISF_EIGHT){
			sb.drawimage(
				"8TOTHE8",(0,-8)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				alpha:0.9,scale:(0.6,0.6)
			);
		}else{
			sb.drawimage(
				"PNFrntST",(0,-8)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				alpha:0.9,scale:(0.6,0.6)
			);
		}
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"PNBackST",(0,-8)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:(0.6,0.6)
		);
	}
	override double weaponbulk(){
		double blx=80;
		int mgg=weaponstatus[VRISS_MAG];
		return blx+(mgg<0?0:(ENC_P90MAG_LOADED+mgg*ENC_9_LOADED));
	}
	override void failedpickupunload(){
		failedpickupunloadmag(VRISS_MAG,"HDNDMBigMag");
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("HDNDMLoose"))owner.A_DropInventory("HDNDMLoose",amt*50);
			else owner.A_DropInventory("HDNDMBigMag",amt);
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("HDNDMLoose");
		owner.A_TakeInventory("HDNDMBigMag");
		owner.A_GiveInventory("HDNDMBigMag");
	}
	override inventory createtossable(){
		let ctt=HDPrincess(super.createtossable());
		if(!ctt)return null;
		if(ctt.bmissile){
			if(ctt.weaponstatus[0]&VRISF_JAMMED&&random(1,7)==7)ctt.weaponstatus[0]&=~VRISF_JAMMED;
			else ctt.weaponstatus[0]|=VRISF_JAMMED;
		}
		return ctt;
	}
	action void A_SpillNDM(){ //spill a single round of NDM
		double fc=max(pitch*0.01,5);
		double cosp=cos(pitch);
		actor ndmsp;
		[cosp,ndmsp]=A_SpawnItemEx("HDNDMLoose",
			cosp*12,0,height-12-sin(pitch)*12,
			cosp*fc,0.2*randompick(-1,1),-sin(pitch)*fc,
			0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
		);
		ndmsp.vel+=vel;
	}
	action void A_CheckPNSprite(){ //>arrive at the gym on PNS inspection day >try to leave >the doors are already locked >mfw
		bool hasmag=invoker.weaponstatus[VRISS_MAG]>=0; //no mag loaded
		if(hasmag)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("BTPNA0");
		else player.getpsprite(PSP_WEAPON).sprite=getspriteindex("BTPEA0");
	}
	states{
	ready:
		BTPN A 0 A_CheckPNSprite();
		#### A 1 A_WeaponReady(WRF_ALL); 
		goto readyend;
	firemode:
		---- A 1{
			if(invoker.weaponstatus[VRISS_AUTO]>=1)invoker.weaponstatus[VRISS_AUTO]=0;  
			else invoker.weaponstatus[VRISS_AUTO]++;
			A_WeaponReady(WRF_NONE);
		}
		goto nope;

	select0:
		BTPN A 0;
		#### A 0 A_CheckPNSprite();
		goto select0big;
	deselect0:
		BTPN A 0;
		#### A 0 A_CheckPNSprite();
		goto deselect0big;
	flash:
		BTPN F 1 bright{
			int lck=invoker.weaponstatus[VRISS_LUCK];
			int roll=random(1,8);
			double spd=1.1;
			if(invoker.weaponstatus[0]&VRISF_VRISKA){
				if(random(1,8)==1){
					invoker.weaponstatus[VRISS_LUCK]--;
//					if(roll==1);
//					if(roll==2);
//					if(roll==3);
//					if(roll==4);
//					if(roll==5);
					if(roll==6)A_DropInventory("HDPrincess");
/*					if(roll==7){
						A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
						A_SpawnItemEx("HDSmokeChunk",12,0,height-12,4,frandom(-2,2),frandom(2,4));
						damagemobj(self,owner,1,"Thermal",DMG_NO_ARMOR);
						A_GiveInventory("wound",8);
					}
*/					if(roll==8)setweaponstate("unjamstart");
				}
				if(random(1,8)==8){
					invoker.weaponstatus[VRISS_LUCK]++;
/*					if(roll==1);
					if(roll==2);
					if(roll==3);
					if(roll==4);
					if(roll==5);
					if(roll==6);
					if(roll==7);
					if(roll==8){
						tg.A_GiveInventory("PowerStrength");
						tg.zerk+=4100;
						tg.haszerked++;
						if(tg.stimcount)tg.aggravateddamage+=int(ceil(tg.stimcount*0.05*random(1,3)));
						else tg.aggravateddamage++;;
					}
*/				}
/*					spd=(8.88/(8/8+8/8)); //BR8K!!!!!!!!
					A_StartSound("weapons/hmpholy",9);
*/			
			}
			A_Light1();
			HDFlashAlpha(-16);
			A_StartSound("weapons/bp90fire",CHAN_WEAPON);
			A_ZoomRecoil(max(0.95,1.-0.05*min(invoker.weaponstatus[VRISS_AUTO],3)));

			double brnd=(invoker.weaponstatus[VRISS_HEAT]);
			HDBulletActor.FireBullet(self,"HDB_NDM",
				spread:1.1,speedfactor:spd
			);

			A_MuzzleClimb(
				-frandom(0.1,0.1),-frandom(0,0.1),
				-0.2,-frandom(0.2,0.3),
				-frandom(0.2,0.8),-frandom(0.7,1.4)
			);
			invoker.weaponstatus[VRISS_FLAGS]&=~VRISF_CHAMBER;
			invoker.weaponstatus[VRISS_LUCK]=0;
			invoker.weaponstatus[VRISS_HEAT]+=random(3,5);
			A_AlertMonsters();
		}
		goto lightdone;
	jam:
		---- A 0{if(!(invoker.weaponstatus[0]&VRISF_CHAMBER)&&invoker.weaponstatus[VRISS_MAG]<1){
				invoker.weaponstatus[0]&=~VRISF_JAMMED;
				setweaponstate("ready");
				}
			}
		---- A 4 offset(-1,36)A_WeaponBusy();
		---- A 8 offset(-3,42){A_StartSound("weapons/bp90click",CHAN_WEAPON);}
		---- A 3 offset(-1,36);
		---- A 0 offset(0,34);
		goto nope;
	fire:
		---- A 0{
			if(invoker.weaponstatus[0]&VRISF_JAMMED)setweaponstate("jam");
			if(invoker.weaponstatus[VRISS_AUTO]>0)A_SetTics(1);
			}
		---- A 2 A_JumpIf(!(invoker.weaponstatus[VRISS_AUTO]),"shootgun");
		---- A 1;
		goto shootgun;
	hold:
		---- A 0 A_JumpIf(invoker.weaponstatus[VRISS_AUTO],"shootgun");
	althold:
		---- A 1;
		---- A 0 A_Refire();
		goto ready;
	shootgun:
		---- A 0;
		---- A 1{
			if(invoker.weaponstatus[0]&VRISF_JAMMED)setweaponstate("jam");
			else if(
					!(invoker.weaponstatus[VRISS_FLAGS]&VRISF_CHAMBER)
					&&invoker.weaponstatus[VRISS_MAG]<1
			){
				setweaponstate("nope");
			}else if(!(invoker.weaponstatus[VRISS_FLAGS]&VRISF_CHAMBER)){
				//no shot but can chamber
				setweaponstate("chamber_manual");
			}else{
				A_GunFlash();
				A_SpawnItemEx("HDSpentNDM");
				A_WeaponReady(WRF_NONE);
			}
		}
	chamber:
		---- A 0 offset(0,32){
			if(invoker.weaponstatus[VRISS_MAG]<1){
				setweaponstate("nope");
				return;
			}
			if(invoker.weaponstatus[VRISS_MAG]>0){  
				invoker.weaponstatus[VRISS_MAG]--;
				invoker.weaponstatus[VRISS_FLAGS]|=VRISF_CHAMBER;
			}else{
				invoker.weaponstatus[VRISS_MAG]=min(invoker.weaponstatus[VRISS_MAG],0);
				A_StartSound("weapons/bp90chamber",CHAN_WEAPON,CHANF_OVERLAP);
			}
			if(random(1,300)<(invoker.weaponstatus[VRISS_HEAT]-1))
				invoker.weaponstatus[0]|=VRISF_JAMMED;
			A_WeaponReady(WRF_NOFIRE);
		}
		---- A 0 A_JumpIf(invoker.weaponstatus[VRISS_AUTO]<1,"nope");
		---- A 2 A_JumpIf(invoker.weaponstatus[VRISS_AUTO]>1,1);
		---- A 0 A_Refire();
		goto ready;

	user3:
		---- A 0 A_MagManager("HDNDMBigMag");
		goto ready;

	user4:
	unload:
		---- A 0{
			if(
				invoker.weaponstatus[VRISS_MAG]>=0  
			){
				setweaponstate("unloadmag");
			}else if(
				invoker.weaponstatus[VRISS_FLAGS]&VRISF_CHAMBER
			){
				setweaponstate("unloadchamber");
			}else{
				setweaponstate("nope");
			}
		}
		goto nope;
	reload:
		---- A 0{
			if(
				invoker.weaponstatus[VRISS_MAG]>=50
			){
				setweaponstate("nope");
			}else if(!HDMagAmmo.NothingLoaded(self,"HDNDMBigMag")
				&&invoker.weaponstatus[VRISS_MAG]<0){
				setweaponstate("loadmag");
			}
			else if(invoker.weaponstatus[VRISS_MAG]<0&&countinv("HDNDMLoose")>0){
				setweaponstate("loadchamber");
			}
		}goto nope;
	unloadmag:
		---- A 1 offset(0,33);
		---- A 1 offset(-3,34);
		---- A 1 offset(-8,37);
		---- A 2 offset(-11,39){
			if(	//no mag, skip unload
				invoker.weaponstatus[VRISS_MAG]<0
			){
				setweaponstate("magout");
			}
		}
		---- A 4 offset(-12,40){
			A_SetPitch(pitch-0.3,SPF_INTERPOLATE);
			A_SetAngle(angle-0.3,SPF_INTERPOLATE);
			A_StartSound("weapons/bp90unload",CHAN_WEAPON);
		}
		---- A 0{if(invoker.weaponstatus[0]&VRISF_JAMMED)setweaponstate("unjamstart");}
		BTPE A 24 offset(-14,44){
			//A_CheckPNSprite();
			int inmag=invoker.weaponstatus[VRISS_MAG];
			invoker.weaponstatus[VRISS_MAG]=-1;
			if(
				!PressingUnload()&&!PressingReload()
				||A_JumpIfInventory("HDNDMBigMag",0,"null")
			){
				HDMagAmmo.SpawnMag(self,"HDNDMBigMag",inmag);
				A_SetTics(1);
			}else{
				HDMagAmmo.GiveMag(self,"HDNDMBigMag",inmag);
				A_StartSound("weapons/pocket",CHAN_WEAPON);
			}
		}
		goto reloadend;
	unjamstart:
		BTPE A 24 offset(-14,44){
			A_WeaponBusy();
			A_SetTics(1);
		}
	spaghettimag: //All of the rounds, all of them
		---- A 1{
			invoker.weaponstatus[VRISS_MAG]--;
			A_SpillNDM();
			A_StartSound("weapons/bp90chamber",8);
		}
		---- A 0{
			if(invoker.weaponstatus[VRISS_MAG]>0)setweaponstate("spaghettimag");
		}
		---- A 1{
			invoker.weaponstatus[0]&=~VRISF_CHAMBER;
			A_SpillNDM();
			A_StartSound("weapons/bp90chamber",8);
		}
		---- A 0{
			HDMagAmmo.SpawnMag(self,"HDNDMBigMag",0);
			invoker.weaponstatus[VRISS_MAG]=-1;
			invoker.weaponstatus[0]&=~VRISF_JAMMED;
		}
		goto nope;
	loadmag:
		---- A 1 offset(0,33);
		---- A 1 offset(-3,34);
		---- A 1 offset(-8,37);
		---- A 2 offset(-11,39);
		---- A 16 offset(-15,45)A_StartSound("weapons/bp90load",CHAN_WEAPON);
		BTPN A 1 offset(-14,44){
			let PNMG=HDNDMBigMag(findinventory("HDNDMBigMag"));
			if(!PNMG){setweaponstate("reloadend");return;}
			invoker.weaponstatus[VRISS_MAG]=PNMG.TakeMag(true);
			A_StartSound("weapons/bp90click2",CHAN_WEAPON);
		}goto reloadend;
	reloadend:
		---- A 2 offset(-11,39);
		---- A 1 offset(-8,37) A_MuzzleClimb(frandom(0.2,-2.4),frandom(0.2,-1.4));
		---- A 1 offset(-3,34);
		goto chamber_manual;

	chamber_manual:
		---- A 0 A_JumpIf(invoker.weaponstatus[VRISS_FLAGS]&VRISF_CHAMBER,"nope");
		---- A 4 offset(-1,36)A_WeaponBusy();
		---- A 8 offset(-3,42){
			if(!invoker.weaponstatus[VRISS_MAG])invoker.weaponstatus[VRISS_MAG]=0;
			if(
				!(invoker.weaponstatus[VRISS_FLAGS]&VRISF_CHAMBER)
				&& !(invoker.weaponstatus[VRISS_FLAGS]&VRISF_CHAMBER)
				&& invoker.weaponstatus[VRISS_MAG]>0
			){
				A_StartSound("weapons/bp90click",CHAN_WEAPON);
				invoker.weaponstatus[VRISS_MAG]--;
				invoker.weaponstatus[VRISS_FLAGS]|=VRISF_CHAMBER;
			}else setweaponstate("nope");
		}
		---- A 3 offset(-1,36);
		---- A 0 offset(0,34);
		goto nope;

	loadchamber:
		---- A 0 A_JumpIf(invoker.weaponstatus[0]&VRISF_CHAMBER,"nope");
		---- A 1 offset(0,36) A_StartSound("weapons/pocket",9);
		---- A 1 offset(2,40);
		---- A 1 offset(2,50);
		---- A 1 offset(3,60);
		---- A 2 offset(5,90);
		---- A 2 offset(7,80);
		---- A 2 offset(10,90);
		---- A 2 offset(8,96);
		---- A 3 offset(6,88){
			if(countinv("HDNDMLoose")){
				A_TakeInventory("HDNDMLoose",1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[0]|=VRISF_CHAMBER;
				A_StartSound("weapons/bp90chamber",8);
			}
		}
		---- A 2 offset(5,76);
		---- A 1 offset(4,64);
		---- A 1 offset(3,56);
		---- A 1 offset(2,48);
		---- A 2 offset(1,38);
		---- A 3 offset(0,34);
		goto readyend;
	unloadchamber:
		---- A 1 offset(-3,34);
		---- A 1 offset(-9,39);
		---- A 3 offset(-19,44) A_MuzzleClimb(frandom(-0.4,0.4),frandom(-0.4,0.4));
		---- A 2 offset(-16,42){
			if(invoker.weaponstatus[0]&VRISF_JAMMED)invoker.weaponstatus[0]&=~VRISF_JAMMED;
			A_MuzzleClimb(frandom(-0.4,0.4),frandom(-0.4,0.4));
			if(
				invoker.weaponstatus[VRISS_FLAGS]&VRISF_CHAMBER
			){
				A_SpawnItemEx("HDNDMLoose",0,0,20,
					random(4,7),random(-2,2),random(-2,1),0,
					SXF_NOCHECKPOSITION
				);
				invoker.weaponstatus[VRISS_FLAGS]&=~VRISF_CHAMBER;
				A_StartSound("weapons/bp90click2",CHAN_WEAPON,CHANF_OVERLAP);
			}
		}goto reloadend;
		
	spawn:
		PNVP ABCD 0;
		---- A 0;
	spawn2:
		---- A -1{
			//set sprite
			sprite=getspriteindex("PNVPA0");

			//set to no-mag frame
			if(invoker.weaponstatus[VRISS_MAG]<0){
				frame=(invoker.weaponstatus[0]&VRISF_VRISKA?3:1);
			}
			else frame=(invoker.weaponstatus[0]&VRISF_VRISKA?2:0);
		}
	}

	override void InitializeWepStats(bool idfa){
		weaponstatus[0]|=VRISF_CHAMBER;
		weaponstatus[0]&=~VRISF_JAMMED;
		weaponstatus[VRISS_MAG]=50;
		weaponstatus[VRISS_LUCK]=0;
		if(!idfa && !owner){
			weaponstatus[VRISS_AUTO]=0; 
			weaponstatus[VRISS_HEAT]=0;
		}
	}
	override void loadoutconfigure(string input){
		weaponstatus[0]|=VRISF_CHAMBER;
		int firemode=getloadoutvar(input,"firemode",1);
		if(firemode>=0){
			weaponstatus[VRISS_AUTO]=clamp(firemode,0,1);
		}
		int eight=getloadoutvar(input,"eightfold",1);
		if(eight>=0)weaponstatus[0]|=VRISF_EIGHT;
		int vris=getloadoutvar(input,"too_l8_to_kiss_me_too_l8_to_kill_me",1);
		if(vris>=0){
			weaponstatus[0]|=VRISF_VRISKA;
			weaponstatus[0]|=VRISF_EIGHT;
		}
	}
}

//None of these values are correct, please edit.
const HDLD_PRNMAG="n50";
class HDNDMBigMag:HDMagAmmo{
	default{
		+hdpickup.norandombackpackspawn;
		hdmagammo.maxperunit 50;
		hdmagammo.roundtype "HDNDMLoose";
		hdmagammo.roundbulk ENC_9_LOADED;
		hdmagammo.magbulk ENC_P90MAG_EMPTY;
		hdmagammo.inserttime 10;
		hdmagammo.extracttime 2;
		scale .5;

		tag "BreakerTek Classic Mag";
		hdpickup.refid HDLD_PRNMAG;
		inventory.pickupmessage "Picked up a BreakerTek Classic Mag.";
	}
	override void GetItemsThatUseThis(){
		itemsthatusethis.push("HDPrincess");
	}
	override string,string,name,double getmagsprite(int thismagamt){
		string magsprite=(thismagamt>0)?"PNMGC0":"PNMGA0";
		return magsprite,"NRNDA0","HDNDMLoose",0.6;
	}
	states{
	spawn:
		PNMG C -1 nodelay;
		stop;
	spawnempty:
		PNDM A -1{
			brollsprite=true;brollcenter=true;
			roll=randompick(0,2)*90;
		}stop;
	}
}
enum princessstatus{
	VRISF_CHAMBER=1,
	VRISF_JAMMED=2,
	VRISF_EIGHT=4,
	VRISF_VRISKA=8,

	VRISS_FLAGS=0,
	VRISS_MAG=1, //-1 is empty
	VRISS_AUTO=2, //2 is burst, 2-5 counts ratchet
	VRISS_HEAT=3,
	VRISS_LUCK=4,
};