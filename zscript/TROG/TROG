// ------------------------------------------------------------
//Tactical Response Operation-Grenade Rifle
//The TRO-G is a business designation for a weapon otherwise known as the Lodyte AR-29. As designed by one Krognar Lodyte, and licensed out to Volt. 
//A military man by trade, and a victim of his own circumstance. One of the earlier and surprisingly reliable adaptations of Volt 4.26 caseless. Too reliable, but not in all of the right ways. 
//The weapon was designed during the Hive City incident, made mostly for combatting the so-called "Tykes" that ran the lower floors of the megacity. Mutated freaks, but not quite demons. 
//4.26 sufficed well for a man and for rudimentary armor, but they would occasionally construct and sortie crude armored walkers (FUN-Es). 
//The frangible rounds wouldn't penetrate very well, but the sheer explosive force of a grenade would topple them over, rendering them immediately useless. 
//As such, the grenade launcher was built right into the weapon, rather than as an attachment. 
//In a bewildering design decision, the trigger group includes the GL and rifle's triggers right next to each other, under one guard. 
//This proved to be as disastrous under pressure as would be expected, so marines started keeping their GLs unloaded to prevent accidents. 
//Lt. Krognar would later be killed in action during the demon invasion, having abandoned his own rifle for the heavier Liberator, he would be found lagging behind the rest of his squad, overburdened with 7.76, when he might have heard the far off cry of "Hell Knight!" from his team ahead of him. 
//A hail of grenades flew over and around him, and he expired cursing out his team for not checking their grenade fire. 
//When pressed for an explanation, SSgt. Eric simply replied "There was a Hell Knight." No further punishment was issued, and the rifle, along with its designer, became a footnote in Volt's history.
// ------------------------------------------------------------
const HDLD_TROG="trg";

const HDCONST_TROGCOOKOFF=21;
class HDTROGRifle:HDWeapon{
	default{

		weapon.selectionorder 25;
		weapon.slotnumber 4;
		weapon.slotpriority 1.7;
		inventory.pickupsound "misc/w_pkup";
		inventory.pickupmessage "Picked up a Tactical Response Operation-Grenade rifle. It belongs in a museum...";
		scale 0.6;
		weapon.bobrangex 0.22;
		weapon.bobrangey 0.9;
		obituary "%o was assaulted by %k.";
		hdweapon.refid HDLD_TROG;
		tag "TRO-G Rifle";
		inventory.icon "TRG1A0";
	}
	override void tick(){
		super.tick();
		drainheat(TROGS_HEAT,12);
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void postbeginplay(){
		super.postbeginplay();
		barrellength=29; //look at the sprites - GL does not extend beyond muzzle
			barrelwidth=1;
			barreldepth=3;
			weaponspecial=1337;
	}
	override double gunmass(){
			return 7.7+weaponstatus[TROGS_MAG]*0.01+(weaponstatus[0]&TROGF_GRENADELOADED?1.:0.);
	}
	override string,double getpickupsprite(){
		string spr;
		spr="TRG1";

		//set to no-mag frame
		if(weaponstatus[TROGS_MAG]<0)spr=spr.."D";
		else spr=spr.."A";

		return spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HDTrogmag")));
			if(nextmagloaded>30){
				sb.drawimage("TMAGA0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM);
			}else if(nextmagloaded<1){
				sb.drawimage("TMAGC0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.);
			}else sb.drawbar(
				"TMAGNORM","TMAGGREY",
				nextmagloaded,30,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawnum(hpl.countinv("HDTrogmag"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			sb.drawimage("ROQPA0",(-62,-4),sb.DI_SCREEN_CENTER_BOTTOM,scale:(0.6,0.6));
			sb.drawnum(hpl.countinv("HDRocketAmmo"),-56,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}

		sb.drawwepcounter(hdw.weaponstatus[TROGS_AUTO],
			-22,-10,"RBRSA3A7","STFULAUT"
		);
		if(hdw.weaponstatus[0]&TROGF_GRENADELOADED)sb.drawrect(-20,-14,4,2.6);
		int lod=clamp(hdw.weaponstatus[TROGS_MAG]%100,0,30);
		sb.drawwepnum(lod,30);
		if(hdw.weaponstatus[0]&TROGF_CHAMBER){
			sb.drawrect(-19,-10,3,1);
			lod++;
		}
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.."  Shoot GL\n"
		..WEPHELP_RELOAD.."  Reload mag\n"
		..WEPHELP_ALTRELOAD.."	Reload GL\n"
		..WEPHELP_FIREMODE.."  Semi/Auto\n"
		..WEPHELP_MAGMANAGER
		..WEPHELP_UNLOAD.." Unload mag \(Hold "..WEPHELP_FIREMODE.." to unload GL\)\n"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		sb.SetClipRect(
			-16+bob.x,-4+bob.y,32,16,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*3;
		bobb.y=clamp(bobb.y,-8,8);
		sb.drawimage(
			"trgftst",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"trgbkst",(0,-7)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP
		);
	}
	override double weaponbulk(){
		double blx=160;
			if(weaponstatus[0]&TROGF_GRENADELOADED)blx+=ENC_ROCKETLOADED;
		int mgg=weaponstatus[TROGS_MAG];
		return blx+(mgg<0?0:(ENC_TROGMAG_LOADED+mgg*ENC_426_LOADED));
	}
	override void failedpickupunload(){
		failedpickupunloadmag(TROGS_MAG,"HDTrogmag");
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("FourMilAmmo"))owner.A_DropInventory("FourMilAmmo",amt*30);
			else{
				double angchange=-10;
				if(angchange)owner.angle-=angchange;
				owner.A_DropInventory("HDTrogmag",amt);
				if(angchange){
					owner.angle+=angchange*2;
					owner.A_DropInventory("HDRocketAmmo",amt);
					owner.angle-=angchange;
				}
			}
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("FourMilAmmo");
		owner.A_TakeInventory("HDTrogmag");
		owner.A_GiveInventory("HDTrogmag");
	}
	states{
	ready:
		TROG A 1 A_WeaponReady(WRF_ALL); 
		goto readyend;
	firemode:
		TROG A 1{
			if(invoker.weaponstatus[TROGS_AUTO]>=1)invoker.weaponstatus[TROGS_AUTO]=0;  
			else invoker.weaponstatus[TROGS_AUTO]++;
			A_WeaponReady(WRF_NONE);
		}
	firemodehold:
		TROG A 1{
			if(pressingunload()
			&&(invoker.weaponstatus[TROGS_FLAGS]&TROGF_GRENADELOADED)){
			if(invoker.weaponstatus[TROGS_AUTO]>=1)invoker.weaponstatus[TROGS_AUTO]=0;  
			else invoker.weaponstatus[TROGS_AUTO]++;
				invoker.weaponstatus[TROGS_FLAGS]|=TROGF_UNLOADONLY;
				setweaponstate("unloadgrenade");
			}else A_WeaponReady(WRF_NONE);
		}
		TROG A 0 A_JumpIf(pressingfiremode()&&(invoker.weaponstatus[TROGS_FLAGS]&TROGF_GRENADELOADED),"firemodehold");
		goto nope;

	select0:
		TROG A 0;
		goto select0big;
	deselect0:
		TROG A 0;
		goto deselect0big;
	flash:
		TRGF A 1 bright{
			A_Light1();
			HDFlashAlpha(-16);
			A_StartSound("weapons/trog",CHAN_WEAPON);
			A_ZoomRecoil(max(0.95,1.-0.05*min(invoker.weaponstatus[TROGS_AUTO],3)));

			//shoot the bullet
			//copypaste any changes to spawnshoot as well!
			double brnd=(invoker.weaponstatus[TROGS_HEAT]);
			HDBulletActor.FireBullet(self,"HDB_426",
				spread:brnd>1.2?brnd:0
			);

			A_MuzzleClimb(
				-frandom(0.1,0.1),-frandom(0,0.1),
				-0.2,-frandom(0.2,0.3),
				-frandom(0.2,0.8),-frandom(0.7,1.4)
			);

			invoker.weaponstatus[TROGS_FLAGS]&=~TROGF_CHAMBER;
			invoker.weaponstatus[TROGS_HEAT]+=random(3,5);
			A_AlertMonsters();
		}
		goto lightdone;


	fire:
		TROG A 0{
			if(invoker.weaponstatus[TROGS_AUTO]>0)A_SetTics(1);
		}goto shootgun;
	hold:
		TROG A 0 A_JumpIf(invoker.weaponstatus[TROGS_AUTO],"shootgun");
	althold:
		---- A 1;
		---- A 0 A_Refire();
		goto ready;
	shootgun:
		TROG A 1{
			if(
					!(invoker.weaponstatus[TROGS_FLAGS]&TROGF_CHAMBER)
					&&invoker.weaponstatus[TROGS_MAG]<1
			){
				setweaponstate("nope");
			}else if(!(invoker.weaponstatus[TROGS_FLAGS]&TROGF_CHAMBER)){
				//no shot but can chamber
				setweaponstate("chamber_manual");
			}else{
				A_GunFlash();
				A_WeaponReady(WRF_NONE);
			}
		}
	chamber:
		TROG B 0 offset(0,32){
			if(invoker.weaponstatus[TROGS_MAG]<1){
				setweaponstate("nope");
				return;
			}
			if(invoker.weaponstatus[TROGS_MAG]%100>0){  
				invoker.weaponstatus[TROGS_MAG]--;
				invoker.weaponstatus[TROGS_FLAGS]|=TROGF_CHAMBER;
			}else{
				invoker.weaponstatus[TROGS_MAG]=min(invoker.weaponstatus[TROGS_MAG],0);
				A_StartSound("weapons/rifchamber",CHAN_WEAPON,CHANF_OVERLAP);
			}
			A_WeaponReady(WRF_NOFIRE); //not WRF_NONE: switch to drop during cookoff
		}
		TROG B 0 A_JumpIf(invoker.weaponstatus[TROGS_AUTO]<1,"nope");
		TROG B 2 A_JumpIf(invoker.weaponstatus[TROGS_AUTO]>1,1);
		TROG B 0 A_Refire();
		goto ready;

	user3:
		TROG A 0 A_MagManager("HDTrogmag");
		goto ready;

	user4:
	unload:
		TROG A 0{
			invoker.weaponstatus[TROGS_FLAGS]|=TROGF_UNLOADONLY;
			if(
				player.cmd.buttons&BT_FIREMODE
				&&countinv("HDRocketAmmo")
			)setweaponstate("unloadgrenade");
			else	if(
				invoker.weaponstatus[TROGS_MAG]>=0  
			){
				setweaponstate("unloadmag");
			}else if(
				invoker.weaponstatus[TROGS_FLAGS]&TROGF_CHAMBER
			){
				setweaponstate("unloadchamber");
			}else{
				setweaponstate("unloadmag");
			}
		}
	reload:
		TROG A 0{
			invoker.weaponstatus[TROGS_FLAGS]&=~TROGF_UNLOADONLY;
			if(	//full mag, no jam, not unload-only - why hit reload at all?
				invoker.weaponstatus[TROGS_MAG]%100>=30
				&&!(invoker.weaponstatus[TROGS_FLAGS]&TROGF_UNLOADONLY)
			){
				setweaponstate("nope");
			}else if(!HDMagAmmo.NothingLoaded(self,"HDTrogmag")){
				setweaponstate("unloadmag");
			}
		}goto nope;
	unloadmag:
		TROG A 1 offset(0,33);
		TROG A 1 offset(-3,34);
		TROG A 1 offset(-8,37);
		TROG B 2 offset(-11,39){
			if(	//no mag, skip unload
				invoker.weaponstatus[TROGS_MAG]<0
			){
				setweaponstate("magout");
			}
		}
		TROG B 4 offset(-12,40){
			A_SetPitch(pitch-0.3,SPF_INTERPOLATE);
			A_SetAngle(angle-0.3,SPF_INTERPOLATE);
			A_StartSound("weapons/trogunload",CHAN_WEAPON);
		}
		TROG B 20 offset(-14,44){
			int inmag=invoker.weaponstatus[TROGS_MAG];
			invoker.weaponstatus[TROGS_MAG]=-1;
			if(
				!PressingUnload()&&!PressingReload()
				||A_JumpIfInventory("HDTrogmag",0,"null")
			){
				HDMagAmmo.SpawnMag(self,"HDTrogmag",inmag);
				A_SetTics(1);
			}else{
				HDMagAmmo.GiveMag(self,"HDTrogmag",inmag);
				A_StartSound("weapons/pocket",CHAN_WEAPON);
			}
		}
	magout:
		TROG B 0{
			if(
				invoker.weaponstatus[TROGS_FLAGS]&TROGF_UNLOADONLY
				||!countinv("HDTrogmag")
			)setweaponstate("reloadend");
		} //fallthrough to loadmag
	loadmag:
		---- A 12{
			let TMAG=HDTrogmag(findinventory("HDTrogmag"));
			if(!TMAG){setweaponstate("reloadend");return;}
			A_StartSound("weapons/pocket",CHAN_WEAPON);
			}
		---- A 2;
	loadmagclean:
		TROG B 8 offset(-15,45)A_StartSound("weapons/trogload",CHAN_WEAPON);
		TROG B 1 offset(-14,44){
			let TMAG=HDTrogmag(findinventory("HDTrogmag"));
			if(!TMAG){setweaponstate("reloadend");return;}
			invoker.weaponstatus[TROGS_MAG]=TMAG.TakeMag(true);
			A_StartSound("weapons/trogclick2",CHAN_WEAPON);
		}goto reloadend;
	reloadend:
		TROG B 2 offset(-11,39);
		TROG A 1 offset(-8,37) A_MuzzleClimb(frandom(0.2,-2.4),frandom(0.2,-1.4));
		TROG A 1 offset(-3,34);
		goto chamber_manual;

	chamber_manual:
		TROG A 0 A_JumpIf(invoker.weaponstatus[TROGS_FLAGS]&TROGF_CHAMBER,"nope");
		TROG A 3 offset(-1,36)A_WeaponBusy();
		TROG B 4 offset(-3,42){
			if(!invoker.weaponstatus[TROGS_MAG]%100)invoker.weaponstatus[TROGS_MAG]=0;
			if(
				!(invoker.weaponstatus[TROGS_FLAGS]&TROGF_CHAMBER)
				&& !(invoker.weaponstatus[TROGS_FLAGS]&TROGF_CHAMBER)
				&& invoker.weaponstatus[TROGS_MAG]%100>0
			){
				A_StartSound("weapons/trogclick",CHAN_WEAPON);
				invoker.weaponstatus[TROGS_MAG]--;
				invoker.weaponstatus[TROGS_FLAGS]|=TROGF_CHAMBER;
			}else setweaponstate("nope");
		}
		TROG A 2 offset(-1,36);
		TROG A 0 offset(0,34);
		goto nope;


	unloadchamber:
		TROG A 1 offset(-3,34);
		TROG A 1 offset(-9,39);
		TROG B 3 offset(-19,44) A_MuzzleClimb(frandom(-0.4,0.4),frandom(-0.4,0.4));
		TROG A 2 offset(-16,42){
			A_MuzzleClimb(frandom(-0.4,0.4),frandom(-0.4,0.4));
			if(
				invoker.weaponstatus[TROGS_FLAGS]&TROGF_CHAMBER
			){
				A_SpawnItemEx("ZM66DroppedRound",0,0,20,
					random(4,7),random(-2,2),random(-2,1),0,
					SXF_NOCHECKPOSITION
				);
				invoker.weaponstatus[TROGS_FLAGS]&=~TROGF_CHAMBER;
				A_StartSound("weapons/trogclick2",CHAN_WEAPON,CHANF_OVERLAP);
			}
		}goto reloadend;

	nadeflash:
		TNT1 A 0 A_JumpIf(invoker.weaponstatus[TROGS_FLAGS]&TROGF_GRENADELOADED,1);
		stop;
		TNT1 A 2{
			invoker.airburst=0;
			A_FireHDGL();
			invoker.weaponstatus[TROGS_FLAGS]&=~TROGF_GRENADELOADED;
			A_StartSound("weapons/grenadeshot",CHAN_WEAPON);
			A_ZoomRecoil(0.95);
		}
		TNT1 A 2 A_MuzzleClimb(
			0,0,0,0,
			-1.2,-3.,
			-1.,-2.8
		);
		stop;


	altfire:
		TROG A 0 A_JumpIf(invoker.weaponstatus[TROGS_FLAGS]&TROGF_GRENADELOADED,1);
		goto nope;
		TROG B 2;
		TROG B 3 A_Gunflash("nadeflash");
		goto nope;


	altreload:
		TROG A 0{
			invoker.weaponstatus[TROGS_FLAGS]&=~TROGF_UNLOADONLY;
			if(
				!(invoker.weaponstatus[TROGS_FLAGS]&TROGF_GRENADELOADED)
				&&countinv("HDRocketAmmo")
			)setweaponstate("unloadgrenade");
		}goto nope;
	unloadgrenade:
		TROG B 0{
			A_SetCrosshair(21);
			A_MuzzleClimb(-0.3,-0.3);
		}
		TROG B 2 offset(0,34);
		TROG B 1 offset(4,38){
			A_MuzzleClimb(-0.3,-0.3);
		}
		TROG B 2 offset(8,48){
			A_StartSound("weapons/troggrenopen",CHAN_WEAPon,CHANF_OVERLAP);
			A_MuzzleClimb(-0.3,-0.3);
			if(invoker.weaponstatus[TROGS_FLAGS]&TROGF_GRENADELOADED)A_StartSound("weapons/grenreload",CHAN_WEAPON);
		}
		TROG B 8 offset(10,49){
			if(!(invoker.weaponstatus[TROGS_FLAGS]&TROGF_GRENADELOADED)){
				if(!(invoker.weaponstatus[TROGS_FLAGS]&TROGF_UNLOADONLY))A_SetTics(3);
				return;
			}
			invoker.weaponstatus[TROGS_FLAGS]&=~TROGF_GRENADELOADED;
			if(
				!PressingUnload()
				||A_JumpIfInventory("HDRocketAmmo",0,"null")
			){
				A_SpawnItemEx("HDRocketAmmo",
					cos(pitch)*10,0,height-10-10*sin(pitch),vel.x,vel.y,vel.z,0,
					SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}else{
				A_StartSound("weapons/pocket",CHAN_WEAPON,CHANF_OVERLAP);
				A_GiveInventory("HDRocketAmmo",1);
				A_MuzzleClimb(frandom(0.8,-0.2),frandom(0.4,-0.2));
			}
		}
		TROG B 0 A_JumpIf(invoker.weaponstatus[TROGS_FLAGS]&TROGF_UNLOADONLY,"greloadend");
	loadgrenade:
		TROG B 2 offset(10,50) A_StartSound("weapons/pocket",CHAN_WEAPON,CHANF_OVERLAP);
		TROG BBB 5 offset(10,50) A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		TROG B 15 offset(8,50){
			A_TakeInventory("HDRocketAmmo",1,TIF_NOTAKEINFINITE);
			invoker.weaponstatus[TROGS_FLAGS]|=TROGF_GRENADELOADED;
			A_StartSound("weapons/troggrenload",CHAN_WEAPON);
		}
	greloadend:
		TROG B 4 offset(4,44) A_StartSound("weapons/troggrenopen",CHAN_WEAPON);
		TROG B 1 offset(0,40);
		TROG A 1 offset(0,34) A_MuzzleClimb(frandom(-2.4,0.2),frandom(-1.4,0.2));
		goto nope;

	spawn:
		TRG1 DA 0;
		---- A 0;
	spawn2:
		---- A -1{
			//set sprite
			sprite=getspriteindex("TRG1A0");

			//set to no-mag frame
			if(invoker.weaponstatus[TROGS_MAG]<0){
				frame=3;
			}
		}
	}
	override inventory CreateTossable(int amt){
		let owner=self.owner;
		let zzz=HDTROGRifle(super.createtossable());
		if(!zzz)return null;
		zzz.target=owner;
		return zzz;
	}

	override void InitializeWepStats(bool idfa){
		weaponstatus[0]|=TROGF_CHAMBER;
		weaponstatus[0]|=TROGF_GRENADELOADED;
		weaponstatus[TROGS_MAG]=30;
		if(!idfa && !owner){
			weaponstatus[TROGS_AUTO]=0;
			weaponstatus[TROGS_HEAT]=0;
		}
	}
	override void loadoutconfigure(string input){
			weaponstatus[0]|=TROGF_CHAMBER;
			int firemode=getloadoutvar(input,"firemode",1);
			int glready=getloadoutvar(input,"glready",1);
			if(!glready){weaponstatus[0]&=~TROGF_GRENADELOADED;}
			else if(glready>=0) {weaponstatus[0]|=TROGF_GRENADELOADED;}
			if(firemode>=0){
				weaponstatus[TROGS_AUTO]=clamp(firemode,0,1);
			}
	}
}

const ENC_TROGMAG=12;
const ENC_TROGMAG_EMPTY=ENC_TROGMAG*0.5;
const ENC_TROGMAG_LOADED=ENC_TROGMAG_EMPTY*0.5;
const HDLD_TROGMAG="430";
class HDTrogmag:HDMagAmmo{
	default{
		//$Category "Ammo/Hideous Destructor/"
		//$Title "4.26mm UAC Standard Mag"
		//$Sprite "CLIPB0"

		hdmagammo.maxperunit 30;  //NOTE: *only* the guns do the "+100 for dirty mag" thing
		hdmagammo.roundtype "FourMilAmmo";
		hdmagammo.roundbulk ENC_426_LOADED;
		hdmagammo.magbulk ENC_TROGMAG_EMPTY;
		hdmagammo.extracttime 8;
		scale .5;

		tag "Vintage 4.26 magazine";
		hdpickup.refid HDLD_TROGMAG;
		inventory.pickupmessage "Picked up a classic-style 30-round 4.26 magazine.";
	}
	override void GetItemsThatUseThis(){
		itemsthatusethis.push("HDTROGRifle");
	}
	override void postbeginplay(){
		super.postbeginplay();
		sealtimer=0;
		breakchance=0;
		
	}
	int breakchance;
	int sealtimer;
	override void doeffect(){
		if(sealtimer>0)sealtimer--;
		if(breakchance>0)breakchance--;
		super.doeffect();
	}
	override string,string,name,double getmagsprite(int thismagamt){
		string magsprite;
		if(thismagamt<1)magsprite="TMAGCMAN";
		else magsprite="TMAGAMAN";
		return magsprite,"RBRSBRN","FourMilAmmo",2.;
	}
		override bool Extract(){
		SyncAmount();
		int mindex=mags.size()-1;
		if(
			mags.size()<1
			||mags[mindex]<1
			||owner.A_JumpIfInventory(roundtype,0,"null")
		)return false;
		extracttime=getdefaultbytype(getclass()).extracttime;
		int totake=min(random(1,24),mags[mindex]);
		if(totake<HDPickup.MaxGive(owner,roundtype,roundbulk))HDF.Give(owner,roundtype,totake);
		else HDPickup.DropItem(owner,roundtype,totake);
		owner.A_StartSound("weapons/rifleclick2",CHAN_WEAPON);
		owner.A_StartSound("weapons/rockreload",CHAN_WEAPON,CHANF_OVERLAP,0.4);
		mags[mindex]-=totake;
		return true;
	}
	override bool Insert(){
		SyncAmount();
		if(
			mags.size()<1
			||mags[mags.size()-1]>=maxperunit
			||!owner.countinv(roundtype)
		)return false;
		owner.A_TakeInventory(roundtype,1,TIF_NOTAKEINFINITE);
		owner.A_StartSound("weapons/rifleclick2",8);
		if(random(0,80)<=breakchance){
			owner.A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
			owner.A_SpawnItemEx("WallChunk",12,0,owner.height-12,4,frandom(-2,2),frandom(2,4));
			breakchance=min(breakchance+15,80);
			return false;
		}
		breakchance=max(breakchance,15);
		owner.A_StartSound("weapons/pocket",9,volume:frandom(0.1,0.6));
		mags[mags.size()-1]++;
		return true;
	}
	override void Consolidate(){
		SyncAmount();
		if(amount<2)return;
		int totalrounds=0;
		int howmanymags=0;
		for(int i=0;i<amount;i++){
			int thismag=mags[i];
			if(
				!thismag
				||thismag>=30
			)continue;
			howmanymags++;
			totalrounds+=mags[i]%30;
			mags[i]=0; //keep the empties, do NOT call clear()!
		}
		if(howmanymags>1)totalrounds=int(totalrounds*frandom(0.9,1.));
		for(int i=0;i<amount;i++){
			if(mags[i]>=30)continue;
			int toinsert=clamp(totalrounds,mags[i],30);
			mags[i]=toinsert;
			totalrounds-=toinsert;
			if(totalrounds<1)break;
		}
	}
	states(actor){
	spawn:
		TMAG AB -1 nodelay{
			int mmm=mags[0];
		}stop;
	spawnempty:
		TMAG C -1{
			brollsprite=true;brollcenter=true;
			roll=randompick(0,0,0,0,2,2,2,2,1,3)*90;
		}stop;
	}
}
enum TROGstatus{
	TROGF_CHAMBER=1,
	TROGF_GRENADELOADED=2,
	TROGF_UNLOADONLY=4,

	TROGS_FLAGS=0,
	TROGS_MAG=1, //-1 is empty
	TROGS_AUTO=2, //2 is burst, 2-5 counts ratchet
	TROGS_HEAT=3,
};

class ClipBoxPickupTrog:IdleDummy{
	override void postbeginplay(){
		super.postbeginplay();
		A_SpawnItemEx("HDTrogmag",flags:SXF_NOCHECKPOSITION);
		A_SpawnItemEx("HDTrogmag",-1,-1,flags:SXF_NOCHECKPOSITION);
		if(random(0,2))A_SpawnItemEx("HDFragGrenadeAmmo",-3,-3,flags:SXF_NOCHECKPOSITION);
		A_SpawnItemEx("HDRocketAmmo",3,3,flags:SXF_NOCHECKPOSITION);

		bool _; Actor wep;
		[_, wep] = A_SpawnItemEx("HDTROGRifle",1,1,flags:SXF_NOCHECKPOSITION);
		HDF.TransferSpecials(self, wep);

		destroy();
	}
}
