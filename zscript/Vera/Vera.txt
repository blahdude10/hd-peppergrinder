// ------------------------------------------------------------
// Vera (Light Vulc)
//The M764 Squad Automatic Weapon is an experimental 4.26 internal rotary machine gun produced by Tech Industries. Despite their efforts to push this weapon towards the UAC or any other large military force, there were basically no takers.
//There was no luck with trying to work a deal out with a major arms producer in attempt to widen distribution. A representative of Volt, Toddrick Howarzson had this to say on the matter: "Another 4.26 support weapon? We have the Vulc already. It just works."
//One small subset of the UAC was interested in using this weapon. The Offensive Demon Sanitization Trooper division thought it would be a good idea to equip their support units with something lighter and easier to manage than the Vulcanette due to their whole modus operandi being to move quickly while retaking compromised areas.
//Whether it be for deep ground surveillance, long range reconnaissance operations, or blitzing key locations occupied by the enemy, having a support weapon with more capacity and reliability than the ZM66 but without the weight and stop and go nature of the Vulcanette was very appealing to the ODST division. They are currently the only force out there utilizing the M764 out in the battlefield.
//The weapon uses a similar system to the Vulcanette, and therefore has many interchangeable parts. The biggest distinction however is that the M764 internalizes most of the mechanisms, making it less likely to gather up any gunk from an exterior source. It's organized in such a fashion to make fixing parts much faster as well.
//However, due to this internalization, the weapon requires maintenance more often due to heat building up inside the weapon on heavy usage. The weapon carries significantly less ammunition than the Vulcanette, which is both a benefit and consequence.
//There is less ammo to fire, but this makes the weapon able to shoulder on the move. A weapon perfect for providing support fire but still being able to stay with the team without lagging behind. 
//We reached out to a seasoned marine who has carried his own M764 for numerous years to hear how he felt about the weapon. He stated: "I love it. Me and Vera have a couple of life lessons for you!" He then showed us a 47 image slideshow of him and the weapon at various exotic locations, on and off the battlefield.
//Suffice to say, some soldiers definitely have quite the attachment to this weapon despite how few may be found out in the wild.
// ------------------------------------------------------------
enum verastatus{
	VERAF_FAST=1,
	VERAF_SPINNINGFAST=2,
	VERAF_JUSTUNLOAD=4,
	VERAF_LOADCELL=8,

	VERAF_DIRTYMAG=16,

	VERAS_MAG1=1,
	VERAS_MAG2=2,
	VERAS_MAG3=3,

	VERAS_CHAMBER1=4,
	VERAS_CHAMBER2=5,
	VERAS_CHAMBER3=6,

	VERAS_BATTERY=7,
	VERAS_ZOOM=8,
	VERAS_HEAT=9,
	VERAS_BREAKCHANCE=10,
};

const HDLD_VERA="m64";
class HDVera:HDWeapon{
	default{
		//$Category "Weapons/Hideous Destructor"
		//$Title "Vulcanette"
		//$Sprite "VULCA0"

		scale 0.8;
		inventory.pickupmessage "Picked up the M764 Squad Automatic Weapon!";
		weapon.selectionorder 40;
		weapon.slotnumber 4;
		weapon.slotpriority 1.4;
		weapon.kickback 24;
		weapon.bobrangex 0.35;
		weapon.bobrangey 1.1;
		weapon.bobspeed 2.5;
		weapon.bobstyle "normal";
		obituary "%o met the budda-budda-budda on the street, and %k killed %h.";
		hdweapon.barrelsize 29,2,3;
		hdweapon.refid HDLD_VERA;
		tag "M764 SAW \"Vera\"";
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override string pickupmessage(){
		string msg=super.pickupmessage();
		int bc=weaponstatus[VERAS_BREAKCHANCE];
		if(bc>50){
			msg.replace("!","");
			msg.replace("the","a");
		}
		if(!bc)msg=msg.." I love it!";
		else if(bc>200)msg=msg..". She's really been through the wringer!";
		else if(bc>100)msg=msg..". She's looking pretty bad.";
		else if(bc>50)msg=msg..". She could use a tune-up.";
		return msg;
	}
	override void tick(){
		super.tick();
		drainheat(VERAS_HEAT,12);
	}
	override inventory createtossable(){
		let ctt=HDVera(super.createtossable());
		if(!ctt)return null;
		if(ctt.bmissile)ctt.weaponstatus[VERAS_BREAKCHANCE]+=random(0,10);
		return ctt;
	}

	override double gunmass(){
		double amt=10+weaponstatus[VERAS_BATTERY]<0?0:1;
		for(int i=VERAS_MAG1;i<=VERAS_MAG3;i++){
			if(weaponstatus[i]>=0)amt+=3.6;
		}
		return amt;
	}
	override double weaponbulk(){
		double blx=175+(weaponstatus[VERAS_BATTERY]>=0?ENC_BATTERY_LOADED:0);
		for(int i=VERAS_MAG1;i<=VERAS_MAG3;i++){
			int wsi=weaponstatus[i];
			if(wsi>=0)blx+=ENC_426_LOADED*wsi+ENC_426MAG_LOADED;
		}
		return blx;
	}
	override string,double getpickupsprite(){return "VRAPA0",1.;}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HD4mMag")));
			if(nextmagloaded>50){
				sb.drawimage("ZMAGA0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(2,2));
			}else if(nextmagloaded<1){
				sb.drawimage("ZMAGC0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.,scale:(2,2));
			}else sb.drawbar(
				"ZMAGNORM","ZMAGGREY",
				nextmagloaded,50,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawbattery(-64,-4,sb.DI_SCREEN_CENTER_BOTTOM,reloadorder:true);
			sb.drawnum(hpl.countinv("HD4mMag"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			sb.drawnum(hpl.countinv("HDBattery"),-56,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		bool bat=hdw.weaponstatus[VERAS_BATTERY]>0;
		for(int i=0;i<3;i++){
			if(i>0&&hdw.weaponstatus[VERAS_MAG1+i]>=0)sb.drawrect(-19-i*4,-14,3,2);
			if(hdw.weaponstatus[VERAS_CHAMBER1+i]>0)sb.drawrect(-15,-14+i*2,1,1);
		}
		sb.drawwepnum(
			hdw.weaponstatus[VERAS_MAG1],
			50,posy:-9
		);
		sb.drawwepcounter(hdw.weaponstatus[0]&VERAF_FAST,
			-28,-16,"blank","STFULAUT"
		);
		if(bat){
			int lod=min(50,hdw.weaponstatus[VERAS_MAG1]);
			if(lod>=0&&hdw.weaponstatus[0]&VERAF_DIRTYMAG)lod=random[shitgun](10,99);
			if(lod>=0)sb.drawnum(lod,-20,-22,
				sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TEXT_ALIGN_RIGHT,Font.CR_RED
			);
			sb.drawwepnum(hdw.weaponstatus[VERAS_BATTERY],20);
		}else if(!hdw.weaponstatus[VERAS_BATTERY])sb.drawstring(
			sb.mamountfont,"00000",(-16,-8),
			sb.DI_TEXT_ALIGN_RIGHT|sb.DI_TRANSLATABLE|sb.DI_SCREEN_CENTER_BOTTOM,
			Font.CR_DARKGRAY
		);
		sb.drawnum(hdw.weaponstatus[VERAS_ZOOM],
			-30,-22,
			sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TEXT_ALIGN_RIGHT,
			Font.CR_DARKGRAY
		);
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_RELOAD.."  Reload mags\n"
		..WEPHELP_ALTRELOAD.."  Reload battery\n"
		..WEPHELP_FIREMODE.."  Switch to "..(weaponstatus[0]&VERAF_FAST?"700":"2100").." RPM\n"
		..WEPHELP_ZOOM.."+"..WEPHELP_FIREMODE.."+"..WEPHELP_UPDOWN.."  Zoom\n"
		..WEPHELP_ZOOM.."+"..WEPHELP_UNLOAD.."  Repair\n"
		..WEPHELP_MAGMANAGER
		..WEPHELP_UNLOADUNLOAD
		..WEPHELP_USE.."+"..WEPHELP_UNLOAD.."  or  "..WEPHELP_USE.."+"..WEPHELP_ALTRELOAD.."  Unload battery\n"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=Screen.GetClipRect();
		sb.SetClipRect(
			-16+bob.x,-10+bob.y,32,20,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*2;
		bobb.y=clamp(bobb.y,-8,8);
		sb.drawimage(
			"veraftst",(0,-8)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.8
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"verabkst",(0,-7)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:(0.95,1.0)
		);
		int scaledyoffset=47;
		if(scopeview){
			double degree=(hdw.weaponstatus[VERAS_ZOOM])*0.1;
			texman.setcameratotexture(hpc,"HDXHCAM3",degree);
			sb.drawimage(
				"HDXHCAM3",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
				scale:(0.31,0.31)
			);
			int scaledwidth=57;
			int cx,cy,cw,ch;
			[cx,cy,cw,ch]=screen.GetClipRect();
			sb.SetClipRect(
				-28+bob.x,19+bob.y,scaledwidth,scaledwidth,
				sb.DI_SCREEN_CENTER
			);
			sb.drawimage(
				"scophole",(0,scaledyoffset)+bob*3,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
				scale:(0.78,0.78)
			);
			sb.SetClipRect(cx,cy,cw,ch);
			sb.drawimage(
				"zm66scop",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
				scale:(0.8,0.8)
			);
			sb.drawstring(
				sb.mAmountFont,string.format("%.1f",degree),
				(6+bob.x,73+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
				Font.CR_BLACK
			);
		}
	}
	override void consolidate(){
		CheckBFGCharge(VERAS_BATTERY);
		if(weaponstatus[VERAS_BREAKCHANCE]>0){
			int bc=weaponstatus[VERAS_BREAKCHANCE];
			int oldbc=bc;
			weaponstatus[VERAS_BREAKCHANCE]=random(bc*2/3,bc);
			if(!owner)return;
			string msg="You try to unwarp some of the parts of your M764";
			if(bc>oldbc)msg=msg..", but only made things worse.";
			else if(bc<oldbc*9/10)msg=msg..". It seems to scroll more smoothly now.";
			else msg=msg..", to little if any avail.";
			owner.A_Log(msg,true);
		}
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("FourMilAmmo"))owner.A_DropInventory("FourMilAmmo",50);
			else{
				owner.angle-=10;
				owner.A_DropInventory("HD4mMag",1);
				owner.angle+=20;
				owner.A_DropInventory("HDBattery",1);
				owner.angle-=10;
			}
		}
	}
		override void postbeginplay(){
		super.postbeginplay();
		weaponspecial=1337;
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("FourMilAmmo");
		owner.A_TakeInventory("HD4mMag");
		owner.A_GiveInventory("HD4mMag",3);
		owner.A_TakeInventory("HDBattery");
		owner.A_GiveInventory("HDBattery");
	}
	states{
	select0:
		VERA A 0;
		goto select0bfg;
	deselect0:
		VERA A 0;
		goto deselect0bfg;


	ready:
		VERA A 1{
			A_SetCrosshair(21);
			if(pressingzoom())A_ZoomAdjust(VERAS_ZOOM,16,70);
			else if(justpressed(BT_FIREMODE|BT_ALTFIRE)){
				invoker.weaponstatus[0]^=VERAF_FAST;
				A_StartSound("weapons/fmswitch",CHAN_WEAPON,CHANF_OVERLAP,0.4);
				A_SetHelpText();
				A_WeaponReady(WRF_NONE);
			}else A_WeaponReady(WRF_ALL);
		}
		goto readyend;

	fire:
		VERA A 1{
			A_WeaponReady(WRF_NONE);
			if(
				invoker.weaponstatus[VERAS_BATTERY]>0 
				&&!random(0,max(0,700-(invoker.weaponstatus[VERAS_BREAKCHANCE]>>1)))
			)invoker.weaponstatus[VERAS_BATTERY]--;
		}goto shoot;
	hold:
		VERA A 0{
			if(invoker.weaponstatus[VERAS_BATTERY]<1)setweaponstate("nope");
		}
	shoot:
		VERA A 2{
			A_WeaponReady(WRF_NOFIRE);
			if(
				invoker.weaponstatus[VERAS_BATTERY]>0    
				&&!random(0,invoker.weaponstatus[0]&VERAF_SPINNINGFAST?200:210)
			)invoker.weaponstatus[VERAS_BATTERY]--;
			invoker.weaponstatus[0]&=~VERAF_SPINNINGFAST;

			//check speed and then shoot
			if(
				invoker.weaponstatus[0]&VERAF_FAST
				&&invoker.weaponstatus[VERAS_BATTERY]>=4
				&&invoker.weaponstatus[VERAS_BREAKCHANCE]<random(100,5000)
			){
				A_SetTics(1);
				invoker.weaponstatus[0]|=VERAF_SPINNINGFAST;
			}else if(invoker.weaponstatus[VERAS_BATTERY]<2){
				A_SetTics(random(3,4));
			}else if(invoker.weaponstatus[VERAS_BATTERY]<3){
				A_SetTics(random(2,3));
			}
			VulcShoot();
			VulcNextRound();
		}
		VERA C 1{
			A_WeaponReady(WRF_NOFIRE);
			//check speed and then shoot
			if(
				invoker.weaponstatus[0]&VERAF_SPINNINGFAST
			){
				A_SetTics(1);
				VulcShoot(true);
				VulcNextRound();
			}else if(invoker.weaponstatus[VERAS_BATTERY]<2){
				A_SetTics(random(3,4));
			}else if(invoker.weaponstatus[VERAS_BATTERY]<3){
				A_SetTics(random(2,3));
			}
		}
		VERA C 1{
			A_WeaponReady(WRF_NONE);
			if(invoker.weaponstatus[VERAS_BATTERY]<1)setweaponstate("spindown");
			else A_Refire("holdswap");
		}goto spindown;
	holdswap:
		VERA A 0{
			if(invoker.weaponstatus[VERAS_MAG1]<1){
				VulcNextMag();
				A_StartSound("weapons/verashunt",CHAN_WEAPON,CHANF_OVERLAP);
			}
		}goto hold;
	spindown:
		VERA B 0{
			A_ClearRefire();
			if(!(invoker.weaponstatus[0]&VERAF_SPINNINGFAST))setweaponstate("nope");
			invoker.weaponstatus[0]&=~VERAF_SPINNINGFAST;
		}
		VERA CB 1{
			A_WeaponReady(WRF_NONE);
			A_MuzzleClimb(frandom(0.2,0.3),-frandom(0.2,0.3));
		}
		VERA BCBCCA 1 A_WeaponReady(WRF_NOFIRE|WRF_NOSWITCH);
		goto ready;


	flash2:
		VERF B 0;
		goto flashfollow;
	flash:
		VERF A 0;
		goto flashfollow;
	flashfollow:
		---- A 0{
			A_MuzzleClimb(0,0,-frandom(0.15,0.45),-frandom(0.6,1.2));
			A_ZoomRecoil(0.99);
			HDFlashAlpha(invoker.weaponstatus[VERAS_HEAT]*48);
		}
		---- A 1 bright A_Light2();
		goto lightdone;


	reload:
		VERA A 0{
			if(
				//abort if all mag slots taken or no spare ammo
				(
					invoker.weaponstatus[VERAS_MAG1]>=0
					&&invoker.weaponstatus[VERAS_MAG2]>=0
					&&invoker.weaponstatus[VERAS_MAG3]>=0
				)
				||!countinv("HD4mMag")
			)setweaponstate("nope");else{
				invoker.weaponstatus[0]&=~(VERAF_JUSTUNLOAD|VERAF_LOADCELL);
				setweaponstate("lowertoopen");
			}
		}
	altreload:
	cellreload:
		VERA A 0{
			int batt=invoker.weaponstatus[VERAS_BATTERY];
			if(
				player.cmd.buttons&BT_USE
			){
				invoker.weaponstatus[0]|=VERAF_JUSTUNLOAD;
				invoker.weaponstatus[0]|=VERAF_LOADCELL;
				setweaponstate("lowertoopen");
				return;
			}else if(
				batt<20
				&&countinv("HDBattery")
			){
				invoker.weaponstatus[0]&=~VERAF_JUSTUNLOAD;
				invoker.weaponstatus[0]|=VERAF_LOADCELL;
				setweaponstate("lowertoopen");
				return;
			}
			setweaponstate("nope");
		}
	unload:
		VERA A 0{
			if(player.cmd.buttons&BT_USE)invoker.weaponstatus[0]|=VERAF_LOADCELL;
			else invoker.weaponstatus[0]&=~VERAF_LOADCELL;
			invoker.weaponstatus[0]|=VERAF_JUSTUNLOAD;
			setweaponstate("lowertoopen");
		}
	//what key to use for cellunload???
	cellunload:
		VERA A 0{
			//abort if no cell to unload
			if(invoker.weaponstatus[VERAS_BATTERY]<0)
			setweaponstate("nope");else{
				invoker.weaponstatus[0]|=VERAF_JUSTUNLOAD;
				invoker.weaponstatus[0]|=VERAF_LOADCELL;
				setweaponstate("uncell");
			}
		}

	//lower the weapon, open it, decide what to do
	lowertoopen:
		VERA A 2 offset(0,36);
		VERA A 2 offset(4,38){
			A_StartSound("weapons/rifleclick2",CHAN_WEAPON);
			A_MuzzleClimb(-frandom(1.2,1.8),-frandom(1.8,2.4));
		}
		VERA A 6 offset(9,41)A_StartSound("weapons/pocket",CHAN_WEAPON);
		VERA A 8 offset(12,43)A_StartSound("weapons/veraopen1",CHAN_WEAPON,CHANF_OVERLAP);
		VERA A 5 offset(10,41)A_StartSound("weapons/veraopen2",CHAN_WEAPON,CHANF_OVERLAP);
		VERA A 0 A_JumpIf(
			invoker.weaponstatus[VERAS_CHAMBER1]<1
			&&invoker.weaponstatus[VERAS_CHAMBER2]<1
			&&invoker.weaponstatus[VERAS_CHAMBER3]<1
			&&invoker.weaponstatus[VERAS_BATTERY]<0
			&&invoker.weaponstatus[VERAS_MAG1]<0
			&&invoker.weaponstatus[VERAS_MAG2]<0
			&&invoker.weaponstatus[VERAS_MAG3]<0
			&&pressingzoom()
			,"openforrepair"
		);
		VERA A 0{
			if(invoker.weaponstatus[0]&VERAF_LOADCELL)setweaponstate("uncell");
			else if(invoker.weaponstatus[0]&VERAF_JUSTUNLOAD)setweaponstate("unmag");
		}goto loadmag;

	uncell:
		VERA A 10 offset(11,42){
			int btt=invoker.weaponstatus[VERAS_BATTERY];
			invoker.weaponstatus[VERAS_BATTERY]=-1;
			if(btt<0)setweaponstate("cellout");
			else if(
				!PressingUnload()
				&&!PressingAltReload()
				&&!PressingReload()
			){
				A_SetTics(4);
				HDMagAmmo.SpawnMag(self,"HDBattery",btt);
				
			}else{
				A_StartSound("weapons/pocket",CHAN_WEAPON);
				HDMagAmmo.GiveMag(self,"HDBattery",btt);
			}
		}goto cellout;

	cellout:
		VERA A 0 offset(10,40) A_JumpIf(invoker.weaponstatus[0]&VERAF_JUSTUNLOAD,"reloadend");
	loadcell:
		VERA A 0{
			let bbb=HDMagAmmo(findinventory("HDBattery"));
			if(bbb)invoker.weaponstatus[VERAS_BATTERY]=bbb.TakeMag(true);
		}goto reloadend;

	reloadend:
		VERA A 3 offset(9,41);
		VERA A 2 offset(6,38);
		VERA A 3 offset(2,34);
	reloadendend:
		VERA A 0 A_JumpIf(!pressingreload()&&!pressingunload(),"ready");
		VERA A 0 A_ReadyEnd();
		VERA A 1 A_WeaponReady(WRF_NONE);
		loop;


	unchamber:
		VERA B 4{
			A_StartSound("weapons/veraextract",CHAN_AUTO,CHANF_DEFAULT,0.3);
			VulcNextRound();
		}VERA A 4;
		VERA A 0 A_JumpIf(PressingUnload(),"unchamber");
		goto nope;
	unmag:
		//if no mags, remove battery
		//if not even battery, remove rounds from chambers
		VERA A 0{
			if(
				invoker.weaponstatus[VERAS_MAG1]<0
				&&invoker.weaponstatus[VERAS_MAG2]<0
				&&invoker.weaponstatus[VERAS_MAG3]<0
			){
				if(invoker.weaponstatus[VERAS_BATTERY]>=0)setweaponstate("cellunload");    
				else setweaponstate("unchamber");
			}
		}
		//first, check if there's a mag2-5.
		//if there's no mag2 but stuff after that, shunt everything over until there is.
		//if there's nothing but mag1, unload mag1.
		VERA A 6 offset(10,40){
			if(
				!invoker.weaponstatus[0]&VERAF_JUSTUNLOAD
			)setweaponstate("loadmag");
			A_StartSound("weapons/rifleload");
			A_MuzzleClimb(-frandom(1.2,1.8),-frandom(1.8,2.4));
		}
	//remove mag #2 first, #1 only if out of options
	unmagpick:
		VERA A 0{
			if(invoker.weaponstatus[VERAS_MAG2]>=0)setweaponstate("unmag2");
			else if(
				invoker.weaponstatus[VERAS_MAG3]>=0
			)setweaponstate("unmagshunt");
			else if(
				invoker.weaponstatus[VERAS_MAG1]>=0    
			)setweaponstate("unmag1");
		}goto reloadend;
	unmagshunt:
		VERA A 0{
			for(int i=VERAS_MAG2;i<VERAS_MAG3;i++){
				invoker.weaponstatus[i]=invoker.weaponstatus[i+1];
			}
			invoker.weaponstatus[VERAS_MAG3]=-1;
			A_StartSound("weapons/verashunt",CHAN_WEAPON,CHANF_OVERLAP);
		}
		VERA AB 2 A_MuzzleClimb(-frandom(0.4,0.6),frandom(0.4,0.6));
		goto ready;

	unmag2:
		VRAP A 0{
			int mg=invoker.weaponstatus[VERAS_MAG2];
			invoker.weaponstatus[VERAS_MAG2]=-1;
			if(mg<0){
				setweaponstate("mag2out");
				return;
			}
			if(
				!PressingUnload()
				&&!PressingReload()
			){
				HDMagAmmo.SpawnMag(self,"HD4mMag",mg);
				setweaponstate("mag2out");
			}else{
				HDMagAmmo.GiveMag(self,"HD4mMag",mg);
				setweaponstate("pocketmag");
			}
		}goto mag2out;
	unmag1:
		VRAP A 0{
			int mg=invoker.weaponstatus[VERAS_MAG1];
			invoker.weaponstatus[VERAS_MAG1]=-1;
			if(mg<0){
				setweaponstate("reloadend");
				return;
			}
			if(
				!PressingUnload()
				&&!PressingReload()
			){
				HDMagAmmo.SpawnMag(self,"HD4mMag",mg);
				setweaponstate("mag2out");
			}else{
				HDMagAmmo.GiveMag(self,"HD4mMag",mg);
				setweaponstate("pocketmag");
			}
		}goto reloadend;
	pocketmag:
		VERA A 0 A_StartSound("weapons/pocket");
		VERA AA 6 A_MuzzleClimb(frandom(0.4,0.6),-frandom(0.4,0.6));
		goto mag2out;
	mag2out:
		VERA A 1{
			for(int i=VERAS_MAG2;i<VERAS_MAG3;i++){
				invoker.weaponstatus[i]=invoker.weaponstatus[i+1];
			}
			invoker.weaponstatus[VERAS_MAG3]=-1;
			A_StartSound("weapons/verashunt",CHAN_WEAPON,CHANF_OVERLAP);
		}
		VERA AB 2 A_MuzzleClimb(-frandom(0.4,0.6),frandom(0.4,0.6));
		VERA A 6 A_JumpIf(invoker.weaponstatus[VERAS_MAG2]<0,"reloadend");
		goto unmag2;

	loadmag:
		//pick the first empty slot and fill that
		VERA A 0 A_StartSound("weapons/pocket");
		VERA AA 6 A_MuzzleClimb(-frandom(0.4,0.6),frandom(-0.4,0.4));
		VERA A 6 offset(10,41){
			if(HDMagAmmo.NothingLoaded(self,"HD4mMag")){setweaponstate("reloadend");return;}
			int lod=HDMagAmmo(findinventory("HD4mMag")).TakeMag(true);

			int magslot=-1;
			for(int i=VERAS_MAG1;i<=VERAS_MAG3;i++){
				if(invoker.weaponstatus[i]<0){
					magslot=i;
					break;
				}
			}
			if(magslot<0){
				setweaponstate("reloadend");
				return;
			}

			if(lod<51){
				if(!random(0,7)){
					A_StartSound("weapons/veraforcemag",CHAN_WEAPON,CHANF_OVERLAP);
					lod=max(0,lod-random(0,1));
					A_Log(HDCONST_426MAGMSG,true);
					if(magslot==VERAS_MAG1)invoker.weaponstatus[0]|=VERAF_DIRTYMAG;
				}
			}else if(magslot==VERAS_MAG1)invoker.weaponstatus[0]&=~VERAF_DIRTYMAG;
			invoker.weaponstatus[magslot]=lod;

			A_MuzzleClimb(-frandom(0.4,0.8),-frandom(0.5,0.7));
		}
		VERA A 8 offset(9,38){
			A_StartSound("weapons/rifleclick",CHAN_WEAPON,CHANF_OVERLAP);
			A_MuzzleClimb(
				-frandom(0.2,0.8),-frandom(0.2,0.3)
				-frandom(0.2,0.8),-frandom(0.2,0.3)
			);
		}
		VERA A 0{
			if(
				(
					PressingReload()
					||PressingUnload()
					||PressingFire()
					||!countinv("HD4mMag")
				)||(
					invoker.weaponstatus[VERAS_MAG1]>=0
					&&invoker.weaponstatus[VERAS_MAG2]>=0
					&&invoker.weaponstatus[VERAS_MAG3]>=0
				)
			)setweaponstate("reloadend");
		}goto loadmag;

	user3:
		VRAP A 0 A_MagManager("HD4mMag");
		goto ready;


	openforrepair:
		VERA A 0{
			let bbb=invoker.weaponstatus[VERAS_BREAKCHANCE];
			string msg="like she's doing just shiny!";
			if(bbb>160)msg="like she's gonna need a closed casket. What kind of freak did this!?";
			else if(bbb>80)msg="like she's about ready for a vacation.";
			else if(bbb>40)msg="like a mess in there. Better get to work.";
			else if(bbb>0)msg="like she could use a tune-up.";
			A_Log("Vera looks "..msg,true);
			A_WeaponBusy();
		}
	readytorepair:
		VERA A 1 offset(11,42){
			if(
				invoker.weaponstatus[VERAS_BREAKCHANCE]<1
				||!pressingzoom()
			){
				setweaponstate("reloadend");
				return;
			}
			if(
				pressingfire()
				||pressingunload()
			){
				if(
					!random(0,5)
					&&invoker.weaponstatus[VERAS_BREAKCHANCE]>0
				){
					invoker.weaponstatus[VERAS_BREAKCHANCE]--;
					A_StartSound("weapons/verafix",CHAN_WEAPONBODY,CHANF_OVERLAP);
					VulcRepairMsg();
				}
				if(hd_debug)A_Log("Break chance: "..invoker.weaponstatus[VERAS_BREAKCHANCE],true);
				switch(random(0,4)){
				case 1:setweaponstate("tryfix1");break;
				case 2:setweaponstate("tryfix2");break;
				case 3:setweaponstate("tryfix3");break;
				default:setweaponstate("tryfix0");break;
				}
			}
		}wait;
	tryfix0:
		VERA B 2 offset(10,43)A_StartSound("weapons/veratryfix",CHAN_WEAPONBODY,CHANF_OVERLAP);
		VERA A 7 offset(11,42)A_MuzzleClimb(0.3,0.3,-0.3,-0.3,0.3,0.3,-0.3,-0.3);
		goto readytorepair;
	tryfix1:
		VERA B 0 A_MuzzleClimb(1,1,-1,-1,1,1,-1,-1);
		VERA B 2 offset(10,43)A_StartSound("weapons/verabelt",CHAN_WEAPONBODY,CHANF_OVERLAP);
		VERA ABABABABABABBAAA 1 offset(11,44);
		goto readytorepair;
	tryfix2:
		VERA B 3 offset(11,43)A_MuzzleClimb(frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1));
		VERA B 7 offset(12,43)A_StartSound("weapons/veratryfix2",CHAN_WEAPONBODY,CHANF_OVERLAP);
		VERA A 7 offset(13,45)A_MuzzleClimb(frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1));
		VERA B 12 offset(14,47)A_MuzzleClimb(frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1));
		VERA BA 2 offset(12,44)A_StartSound("weapons/veratryfix2",CHAN_WEAPONBODY,CHANF_OVERLAP);
		VERA B 7 offset(12,43);
		goto readytorepair;
	tryfix3:
		VERA B 0 A_MuzzleClimb(1,1,-1,-1,1,1,-1,-1);
		VERA B 1 offset(11,45);
		VERA B 1 offset(11,48)A_StartSound("weapons/veratryfix1",CHAN_WEAPONBODY,CHANF_OVERLAP);
		VERA B 1 offset(12,54);
		VERA B 0 A_MuzzleClimb(1,1,-1,-1,1,1,-1,-1);
		VERA B 3 offset(15,58);
		VERA B 2 offset(14,56);
		VERA B 1 offset(12,52);
		VERA B 1 offset(11,50);
		VERA B 1 offset(10,48);
		goto readytorepair;


	spawn:
		VRAP A -1;
	}


	override void InitializeWepStats(bool idfa){
		weaponstatus[VERAS_BATTERY]=20;
		weaponstatus[VERAS_ZOOM]=30;
		weaponstatus[VERAS_MAG1]=51;
		weaponstatus[VERAS_MAG2]=51;
		weaponstatus[VERAS_MAG3]=51;
		int chm=idfa?1:0;
		weaponstatus[VERAS_CHAMBER1]=chm;
		weaponstatus[VERAS_CHAMBER2]=chm;
		weaponstatus[VERAS_CHAMBER3]=chm;
		weaponstatus[0]&=~VERAF_DIRTYMAG;
	}
	override void loadoutconfigure(string input){
		int fast=getloadoutvar(input,"fast",1);
		if(!fast)weaponstatus[0]&=~VERAF_FAST;
		else if(fast>0)weaponstatus[0]|=VERAF_FAST;

		int zoom=getloadoutvar(input,"zoom",3);
		if(zoom>=0)weaponstatus[VERAS_ZOOM]=clamp(zoom,16,70);
	}

	//shooting and cycling actions
	//move this somewhere sensible
	action void VulcShoot(bool flash2=false){
		invoker.weaponstatus[VERAS_BREAKCHANCE]+=random(0,random(0,invoker.weaponstatus[VERAS_HEAT]/192));

		int ccc=invoker.weaponstatus[VERAS_CHAMBER1];
		if(ccc<1)return;
		if(ccc>1){
			invoker.weaponstatus[VERAS_BREAKCHANCE]+=random(0,7);
			if(hd_debug)A_Log("Break chance: "..invoker.weaponstatus[VERAS_BREAKCHANCE]);
			return;
		}

		if(random(random(1,500),5000)<invoker.weaponstatus[VERAS_BREAKCHANCE]){
			setweaponstate("nope");
			return;
		}
		if(!random(0,150))invoker.weaponstatus[VERAS_BREAKCHANCE]++;

		if(flash2)A_GunFlash("flash2");else A_GunFlash("flash");
		A_StartSound("weapons/vera",CHAN_WEAPON,CHANF_OVERLAP);
		A_AlertMonsters();

		int cm=countinv("IsMoving");if(
			invoker.weaponstatus[0]&VERAF_FAST
			&&!countinv("PowerStrength")
		)cm*=2;
		double offx=frandom(-0.1,0.1)*cm;
		double offy=frandom(-0.1,0.1)*cm;

		int heat=min(50,invoker.weaponstatus[VERAS_HEAT]);
		HDBulletActor.FireBullet(self,"HDB_426",zofs:height-8,
			spread:heat>20?heat*0.1:0,
			distantsound:"world/vulcfar"
		);
		invoker.weaponstatus[VERAS_HEAT]+=4;

		if(random(0,8192)<min(10,heat))invoker.weaponstatus[VERAS_BATTERY]++;

		invoker.weaponstatus[VERAS_CHAMBER1]=0;
	}
	action void VulcNextRound(){
		int thisch=invoker.weaponstatus[VERAS_CHAMBER1];
		if(thisch>0){
			//spit out a misfired, wasted or broken round
			if(thisch>1){
				for(int i=0;i<5;i++){
					A_SpawnItemEx("TinyWallChunk",3,0,height-18,
						random(4,7),random(-2,2),random(-2,1),
						-30,SXF_NOCHECKPOSITION
					);
				}
			}else{
				A_SpawnItemEx("ZM66DroppedRound",3,0,height-18,
					random(4,7),random(-2,2),random(-2,1),
					-30,SXF_NOCHECKPOSITION
				);
			}
			A_MuzzleClimb(frandom(0.6,2.4),frandom(1.2,2.4));
		}

		//cycle all chambers
		for(int i=VERAS_CHAMBER1;i<VERAS_CHAMBER3;i++){
			invoker.weaponstatus[i]=invoker.weaponstatus[i+1];
		}

		//check if mag is clean
		int inmag=invoker.weaponstatus[VERAS_MAG1];
		if(inmag==51){
			invoker.weaponstatus[VERAS_MAG1]=50; //open the seal
			invoker.weaponstatus[0]&=~VERAF_DIRTYMAG;
			inmag=50;
		}

		//extract a round from the mag
		if(inmag>0){
			invoker.weaponstatus[VERAS_MAG1]--;
			A_StartSound("weapons/verachamber",CHAN_WEAPON,CHANF_OVERLAP);
			if(random(0,2000)<=
				1+(invoker.weaponstatus[0]&VERAF_DIRTYMAG?(invoker.weaponstatus[0]&VERAF_FAST?13:9):0)
			)invoker.weaponstatus[VERAS_CHAMBER3]=2;
			else invoker.weaponstatus[VERAS_CHAMBER3]=1;
		}else invoker.weaponstatus[VERAS_CHAMBER3]=0;
	}
	action void VulcNextMag(){
		int thismag=invoker.weaponstatus[VERAS_MAG1];
		if(thismag>=0){
			double cp=cos(pitch);double ca=cos(angle+60);
			double sp=sin(pitch);double sa=sin(angle+60);
			actor mmm=HDMagAmmo.SpawnMag(self,"HD4mMag",thismag);
			mmm.setorigin(pos+(
				cp*ca*16,
				cp*sa*16,
				height-12-12*sp
			),false);
			mmm.vel=vel+(
				cp*cos(angle+random(55,65)),
				cp*sin(angle+random(55,65)),
				sp
			);
		}
		for(int i=VERAS_MAG1;i<VERAS_MAG3;i++){
			invoker.weaponstatus[i]=invoker.weaponstatus[i+1];
		}
		invoker.weaponstatus[VERAS_MAG3]=-1;

		if(invoker.weaponstatus[VERAS_MAG1]<51)invoker.weaponstatus[0]|=VERAF_DIRTYMAG;
	}

	action void VulcRepairMsg(){
		static const string vordinals[]={"first","second","third"};
		static const string vverbs[]={"remove some","buff out","realign","secure","grease","grab a spare part to replace","fiddle around and eventually suspect a problem with","forcibly un-warp","reassemble"};
		static const string vdebris[]={"debris","grease","dust","steel filings","powder","blood","pus","hair","dead insects","blueberry jam","cheese puff powder","tiny Bosses"};
		static const string vpart[]={"crank shaft","main gear","magazine feeder","mag scanner head","barrel shroud","cylinder","cylinder feed port","motor power feed","CPU auxiliary power turbine","misfire ejector lug"};
		static const string vpart2[]={"barrel feed port","chamber","extractor","extruder","barrel feed port seal","barrel","transfer gear","firing pin","safety scanner"};

		string msg="You ";
		if(!random(0,3))msg=msg.."attempt to ";
		int which=random(0,vverbs.size()-1);
		msg=msg..vverbs[which].." ";
		if(!which)msg=msg..vdebris[abs(random(1,vdebris.size())-random(1,vdebris.size()))].." from ";
		msg=msg.."the ";

		which=random(0,vpart.size());
		if(which==vpart.size())msg=msg..vordinals[random(0,2)].." "..vpart2[random(0,vpart2.size()-1)];
		else msg=msg..vpart[which];

		A_Log(msg..".",true);
	}
}
